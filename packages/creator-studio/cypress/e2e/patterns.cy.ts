describe('Patterns', () => {
   before(() => {
      cy.resetDocker()
   })

   it('creates blueprint from pattern', () => {
      cy.authorize()
      cy.contains(/fortnite/i).click()
      cy.findByRole('button', { name: /space overview/i }).click()

      cy.findAllByTestId('pattern-card')
         .contains('header', 'skin')
         .findByRole('button', { name: /create new/i })
         .click()
      cy.findByLabelText(/name/i, { selector: 'input' }).clear().type('Skin test')
      cy.findByLabelText(/description/i, { selector: 'textarea' })
         .clear()
         .type('Skin description')
      cy.findByTestId('interpretation-file-upload').selectFile('cypress/fixtures/skin.jpg', {
         action: 'drag-drop',
      })
      // eslint-disable-next-line cypress/no-unnecessary-waiting
      cy.wait(1000)
      cy.findByRole('button', { name: /submit/i }).click()

      cy.url().should('include', '/blueprints')
      cy.findByTestId('blueprint-list').contains('Skin test').should('be.visible')
   })
})

export {}
