describe('Authorize', () => {
   before(() => {
      cy.resetDocker()
   })

   it('authorizes', () => {
      cy.authorize()

      cy.contains(/select space/i).should('be.visible')
   })
})

export {}
