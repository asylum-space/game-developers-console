import '@testing-library/cypress/add-commands'

Cypress.Commands.add('authorize', () => {
   cy.visit('/')

   cy.findByRole('button', { name: /disconnected/i }).click()
   cy.findByTestId('modal')
      .findByRole('button', { name: /^connect$/i })
      .click()

   cy.contains(/local node/i)
   cy.contains('connect wallet').click()
})

Cypress.Commands.add('resetDocker', () => {
   cy.exec('docker-compose restart node-asylum')

   if (Cypress.platform === 'win32') {
      cy.exec(
         'docker-compose exec ipfs ipfs config --json API.HTTPHeaders.Access-Control-Allow-Origin "[\\"*\\"]"'
      )
   } else {
      cy.exec(
         'docker-compose exec -T ipfs ipfs config --json API.HTTPHeaders.Access-Control-Allow-Origin \'["*"]\''
      )
   }

   cy.exec('docker-compose restart ipfs')
   // eslint-disable-next-line cypress/no-unnecessary-waiting
   cy.wait(1000)
   cy.exec('cd ../../ && yarn seed')
})

declare global {
   // eslint-disable-next-line no-unused-vars
   namespace Cypress {
      interface Chainable {
         resetDocker(): Chainable<void>
         authorize(): Chainable<void>
      }
   }
}

export {}
