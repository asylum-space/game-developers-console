import axios from 'axios'
import { FormikProps, setNestedObjectValues } from 'formik'
import { find, intersection, keys, map } from 'lodash/fp'

import {
   AsylumApi,
   CID,
   InterpretationPattern,
   Pattern,
   TagMetadata,
} from '@asylum-ui/connection-library'

import { InterpretationUnwrapped } from 'types'

/**
 * Formats address from blockchain to human-readable short format.
 * @param address blockchain address or similar string
 */
export const formatAddress = (address: string) => {
   return `${address.substring(0, 4).toUpperCase()}...${address
      .substring(address.length - 4)
      .toUpperCase()}`
}

export const ipfsPath = (cid?: CID): string => {
   return cid ? `${AsylumApi.ipfsUrl?.replace(':5001', ':8080')}/ipfs/${cid}` : ''
}

export const getFile = async (cid: CID) => {
   const { data } = await axios.get(ipfsPath(cid))
   return data
}

export const getAllFiles = async (cidArray: CID[]) => {
   const result = await Promise.all(cidArray.map((cid) => axios.get(ipfsPath(cid))))
   return map('data', result)
}

export const generateTagMetadata = (
   tags: TagMetadata[],
   editableTagFields: {
      [key: string]: string
   }
) => {
   const conflictedFields = new Set<string>()
   const conflictedTags = new Set<TagMetadata>()
   return {
      metadata: tags.slice(0).reduce(
         (result, tag) => {
            const fields = tag.metadataExtensions.fields.reduce((result, field) => {
               let value: string | number | null =
                  editableTagFields[field.name] !== undefined
                     ? editableTagFields[field.name]
                     : field.defaultValue ?? null
               if (field.type === 'number' && value !== null) {
                  value = parseFloat(value)
               }
               return {
                  ...result,
                  [field.name]: value,
               }
            }, {})

            const intersections = intersection(keys(fields), keys(result))
            if (intersections.length !== 0) {
               intersections.forEach((field) => {
                  conflictedFields.add(field)
               })

               conflictedTags.add(
                  find(
                     (tag) =>
                        intersection(map('name', tag.metadataExtensions.fields), intersections)
                           .length,
                     tags
                  ) as TagMetadata
               )
               conflictedTags.add(tag)

               return result
            }
            return {
               ...result,
               ...fields,
               description: result.description
                  ? `${result.description} | ${tag.description}`
                  : tag.description,
            }
         },
         { description: '' }
      ),
      conflictedFields: Array.from(conflictedFields.values()),
      conflictedTags: Array.from(conflictedTags.values()),
   }
}

export const findEditableFields = (tag: TagMetadata | TagMetadata[]) => {
   if (Array.isArray(tag)) {
      return tag
         .map((tag) => tag.metadataExtensions.fields.filter((field) => field.editable))
         .flat()
   }
   return tag.metadataExtensions.fields.filter((field) => field.editable)
}

export const findDefaultTag = (tags: TagMetadata[]) =>
   tags?.find((tag) => tag.id === 'default-view')

export const touchAndValidate = async (formik: FormikProps<any>) => {
   // Used for submission mimicry but without actual submission
   const errors = await formik.validateForm()
   const touched = { ...formik.touched, ...(setNestedObjectValues(errors, true) as object) }
   if (touched.tags) {
      touched.tags = true
   }
   formik.setTouched(touched)
}

export function isMatchingPattern<T extends { interpretations: InterpretationUnwrapped[] }>(
   pattern: Pattern,
   data: T
): boolean {
   for (const patternInterpretation of pattern.interpretations) {
      let testedInterpretations = data.interpretations

      if (patternInterpretation.tags) {
         testedInterpretations = testedInterpretations.filter((blueprintInterpretation) =>
            // isEqual(blueprintInterpretation.tags.sort(), patternInterpretation.tags!.sort())
            patternInterpretation.tags!.every((tag) => blueprintInterpretation.tags!.includes(tag))
         )
      }

      if (patternInterpretation.fields) {
         testedInterpretations = testedInterpretations.filter((blueprintInterpretation) => {
            for (const field of patternInterpretation.fields!) {
               if (blueprintInterpretation.interpretationInfo.metadata[field.name] === undefined) {
                  return false
               }

               if (
                  field.value !== undefined &&
                  blueprintInterpretation.interpretationInfo.metadata[field.name] !== field.value
               ) {
                  return false
               }
            }

            return true
         })
      }

      if (testedInterpretations.length === 0) return false
   }
   return true
}

export function isMatchingSomePatterns<T extends { interpretations: InterpretationUnwrapped[] }>(
   patterns: Pattern[],
   data: T
): boolean {
   return patterns.some((pattern) => isMatchingPattern(pattern, data))
}

export function filterMatchedPattern<T extends { interpretations: InterpretationUnwrapped[] }>(
   pattern: Pattern,
   dataArray: T[]
): T[] {
   return dataArray.filter((data) => isMatchingPattern(pattern, data))
}

export function filterMatchedSomePatterns<T extends { interpretations: InterpretationUnwrapped[] }>(
   patterns: Pattern[],
   dataArray: T[]
): T[] {
   return dataArray.filter((data) => patterns.some((pattern) => isMatchingPattern(pattern, data)))
}

export function isMatchingInterpretation(
   interpretation: InterpretationUnwrapped,
   interpretationPattern: InterpretationPattern
): boolean {
   if (
      interpretationPattern.tags &&
      // !isEqual(interpretation.tags.sort(), interpretationPattern.tags!.sort())
      !interpretationPattern.tags.every((tag) => interpretation.tags.includes(tag))
   ) {
      return false
   }

   if (interpretationPattern.fields) {
      for (const field of interpretationPattern.fields!) {
         if (interpretation.interpretationInfo.metadata[field.name] === undefined) {
            return false
         }

         if (
            field.value !== undefined &&
            interpretation.interpretationInfo.metadata[field.name] !== field.value
         ) {
            return false
         }
      }
   }
   return true
}
