import { useState } from 'react'

import { observer } from 'mobx-react-lite'
import { useQuery } from 'react-query'

import { fetchBlueprints, fetchItems } from 'api'
import { FilterButton } from 'components/button'
import { GridItems } from 'components/grid-items'
import { ListCardItem, ListCardItemSkeleton } from 'components/list-card-item'
import { Page } from 'components/page'
import { SearchAutocomplete } from 'components/search-autocomplete'
import { Paragraph } from 'components/text'
import { FilterModal } from 'modules/modal'
import { useStore } from 'store'
import { BlueprintUnwrapped, ItemUnwrapped } from 'types'

export const Items = observer(() => {
   const store = useStore()
   const {
      data: items = [],
      isLoading,
      isSuccess,
   } = useQuery('items', () => (store.account ? fetchItems(store.account.address) : []))
   const { data: blueprints, isSuccess: isBlueprintLoaded } = useQuery(
      'blueprints',
      fetchBlueprints
   )

   const [selectedBlueprints, setSelectedBlueprints] = useState<BlueprintUnwrapped[]>([])
   const [blueprintFilterOpen, setBlueprintFilterOpen] = useState(false)
   const [query, setQuery] = useState('')

   const filterItemsByBlueprint = (items: ItemUnwrapped[]): ItemUnwrapped[] => {
      const BlueprintIds = selectedBlueprints.map((blueprint) => blueprint.id)
      return items.filter((item) => BlueprintIds.includes(item.blueprintId))
   }

   const filterItemsByQuery = (items: ItemUnwrapped[]): ItemUnwrapped[] => {
      return items.filter((item) => item.metadata.name.toLowerCase().includes(query.toLowerCase()))
   }

   const filteredItemsByBlueprint = selectedBlueprints.length
      ? filterItemsByBlueprint(items)
      : items

   const filteredItemsByQuery = filterItemsByQuery(filteredItemsByBlueprint)

   return (
      <>
         <Page title="Items">
            <div className="flex gap-5 my-10">
               <SearchAutocomplete
                  className="grow"
                  variants={filteredItemsByBlueprint
                     .map((item) => item.metadata.name)
                     .filter((value, index, self) => self.indexOf(value) === index)}
                  onVariantSelect={setQuery}
               />
               <FilterButton
                  filteredData={selectedBlueprints}
                  onClick={() => setBlueprintFilterOpen(true)}
               />
            </div>
            <GridItems>
               {isLoading && [...Array(6)].map((_, i) => <ListCardItemSkeleton key={i} />)}
               {isSuccess &&
                  filteredItemsByQuery.map((item) => {
                     const key = `${item.id}${item.blueprintId}`
                     return (
                        <ListCardItem
                           to={`${item.blueprintId}_${item.id}`}
                           title={item.metadata.name}
                           id={key}
                           interpretation={item.defaultInterpretation}
                           description={item.metadata.description}
                           address={item.owner.AccountId}
                           key={key}
                           pending={item.hasPendingInterpretations}
                        />
                     )
                  })}
            </GridItems>
            {query && filteredItemsByQuery.length === 0 && (
               <Paragraph className="text-white">
                  Your search - <strong>{query}</strong> - did not match any Blueprints.
               </Paragraph>
            )}
            {isSuccess && items.length === 0 && (
               <Paragraph className="text-white">No Items associated with your account.</Paragraph>
            )}
         </Page>
         {isBlueprintLoaded && (
            <FilterModal
               title="Filter items"
               values={blueprints}
               valuesName="blueprints"
               initialValue={selectedBlueprints}
               optionSelector={(blueprint: BlueprintUnwrapped) => blueprint.name}
               onApply={setSelectedBlueprints}
               open={blueprintFilterOpen}
               onClose={() => setBlueprintFilterOpen(false)}
            />
         )}
      </>
   )
})
