import { Card } from 'components/card'
import { Gallery } from 'components/gallery'
import { Heading, HeadingLg, Paragraph } from 'components/text'
import { SpaceWithMetadataUnwrapped } from 'types'
import { ipfsPath } from 'utils'

interface Props {
   space: SpaceWithMetadataUnwrapped
}

export const SpaceDescription = ({ space }: Props) => {
   return (
      <Card header={<HeadingLg>{space.title}</HeadingLg>} contentClassName="flex gap-9 pt-8">
         <>
            <div className="basis-7/12">
               <Gallery>
                  {space.gallery.slice(0, 5).map((imgSrc, index) => (
                     <div key={index}>
                        <img
                           src={ipfsPath(imgSrc)}
                           alt={space.title}
                           className="aspect-video object-cover object-center"
                        />
                     </div>
                  ))}
               </Gallery>
            </div>
            <div className="basis-5/12">
               <Heading className="mb-4">Description</Heading>
               {space.description.split('\n').map((paragraph, index) => (
                  <Paragraph key={index} className="mb-3">
                     {paragraph}
                  </Paragraph>
               ))}
            </div>
         </>
      </Card>
   )
}
