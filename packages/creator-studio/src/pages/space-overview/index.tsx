import { observer } from 'mobx-react-lite'

import { Page } from 'components/page'
import { Paragraph } from 'components/text/paragraph'
import { Reviews } from 'pages/space-overview/reviews'
import { SupportedPatterns } from 'pages/space-overview/supported-patterns'
import { useStore } from 'store'

import { Details } from './details'
import { SpaceDescription } from './space-description'

export const SpaceOverview = observer(() => {
   const store = useStore()

   if (!store.selectedSpace) {
      return <Paragraph className="text-white">Space is not selected</Paragraph>
   }

   return (
      <Page title="Space overview" className="flex flex-col gap-10">
         <SpaceDescription space={store.selectedSpace} />
         <Details space={store.selectedSpace} />
         {store.selectedSpace.patterns && (
            <SupportedPatterns patterns={store.selectedSpace.patterns} />
         )}
         <Reviews reviews={store.selectedSpace.reviews} />
      </Page>
   )
})
