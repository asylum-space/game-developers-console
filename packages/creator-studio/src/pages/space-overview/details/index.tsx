import { useQuery } from 'react-query'
import { useNavigate } from 'react-router-dom'

import { fetchBlueprints, fetchParticipantsCount } from 'api'
import { Button } from 'components/button'
import { Card } from 'components/card'
import { Heading } from 'components/text/heading'
import { HeadingLg } from 'components/text/heading-lg'
import { Paragraph } from 'components/text/paragraph'
import { ComponentProps, SpaceWithMetadataUnwrapped } from 'types'
import { filterMatchedSomePatterns } from 'utils'

interface Props {
   space: SpaceWithMetadataUnwrapped
}

export const Details = ({ space }: ComponentProps<Props>) => {
   const navigate = useNavigate()
   const { data: participantsCount } = useQuery('participantsCount', () => fetchParticipantsCount())
   const { data: blueprints = [] } = useQuery('blueprints', fetchBlueprints)

   const spaceBlueprints = filterMatchedSomePatterns(space.patterns ?? [], blueprints)

   const mintedItems = spaceBlueprints.reduce((acc, blueprint) => acc + blueprint.nftsCount, 0) || 0

   return (
      <Card
         header={<HeadingLg>Details</HeadingLg>}
         contentClassName="flex flex-row justify-between"
      >
         <>
            <div className="grow">
               <Heading>Number of participants:</Heading>
               <Paragraph className="font-secondary">{participantsCount}</Paragraph>
            </div>
            <div className="grow">
               <Heading>Minted items:</Heading>
               <Paragraph className="font-secondary">{mintedItems}</Paragraph>
            </div>
            <div className="grow">
               <Heading>Associated blueprints:</Heading>
               <Paragraph className="font-secondary">{spaceBlueprints.length}</Paragraph>
            </div>
            <Button
               onClick={() => navigate('/blueprints', { state: { blueprintIds: [space.id] } })}
            >
               show all blueprints
            </Button>
         </>
      </Card>
   )
}
