import { useContext } from 'react'

import { OptionProps, SingleValueProps, components } from 'react-select'

import { InterpretationPattern } from '@asylum-ui/connection-library'

import { ReactComponent as CheckmarkIcon } from 'assets/svg/checkmark.svg'
import { InputSelect, InputSelectProps } from 'components/input-select'
import { Heading, Paragraph } from 'components/text'
import { ColorContext } from 'context'
import { BlueprintUnwrapped } from 'types'
import { ipfsPath } from 'utils'

export interface BlueprintUnwrappedForSelect extends BlueprintUnwrapped {
   unresolved: InterpretationPattern[]
}

interface PatternInputSelectProps extends InputSelectProps<BlueprintUnwrappedForSelect> {
   blueprints: BlueprintUnwrappedForSelect[]
}

export const PatternInputSelect = ({ blueprints, ...rest }: PatternInputSelectProps) => {
   const colorContextValue = useContext(ColorContext)
   const compareBlueprints = (a: BlueprintUnwrappedForSelect, b: BlueprintUnwrappedForSelect) => {
      if (!a.unresolved.length && !b.unresolved.length) return 0
      if (!a.unresolved.length) return 1
      if (!b.unresolved.length) return -1
      return a.unresolved.length - b.unresolved.length
   }

   return (
      <InputSelect<BlueprintUnwrappedForSelect>
         isMulti={false}
         surroundColor={colorContextValue}
         name="blueprints"
         options={blueprints.sort(compareBlueprints)}
         styles={{
            option: (base, state) => ({
               ...base,
               display: 'flex',
               justifyContent: 'space-between',
               alignItems: 'center',
               color: 'white',
               transition: 'background-color 0.15s',
               backgroundColor: state.isSelected ? '#404040' : '',
               paddingLeft: '0',
               paddingTop: '1rem',
               paddingBottom: '1rem',
               cursor: 'pointer',
               '&:hover': {
                  backgroundColor: state.isSelected ? '#404040' : '#262626',
               },
            }),
            singleValue: (base) => ({
               ...base,
               whiteSpace: 'pre-wrap',
               display: 'flex',
               justifyContent: 'space-between',
               alignItems: 'center',
               color: 'white',
            }),
            noOptionsMessage: (base) => ({
               ...base,
               marginTop: '1.875rem',
               marginBottom: '1.875rem',
            }),
         }}
         getOptionLabel={(blueprint) => blueprint.name}
         getOptionValue={(blueprint) => blueprint.id}
         components={{
            SingleValue,
            Option,
         }}
         {...rest}
      />
   )
}

const SingleValue = (props: SingleValueProps<BlueprintUnwrappedForSelect>) => {
   return (
      <components.SingleValue {...props}>
         <div className="flex py-2 items-center basis-11/12">
            <img
               src={ipfsPath(props.data.img)}
               alt={props.data.name}
               className="rounded-full h-[4.375rem] aspect-square object-contain m-auto mr-4"
            />
            <div className="flex flex-col gap-0.5 grow">
               <Heading>{props.data.name}</Heading>
               <Paragraph className="text-neutral-400 line-clamp-2">
                  {props.data.metadata.description}
               </Paragraph>
            </div>
         </div>
         <div>
            {!props.data.unresolved.length ? (
               <CheckmarkIcon className="fill-white" />
            ) : (
               <span className="text-amber-300">{props.data.unresolved.length} missed</span>
            )}
         </div>
      </components.SingleValue>
   )
}

const Option = (props: OptionProps<BlueprintUnwrappedForSelect>) => {
   return (
      <components.Option {...props}>
         <div className="flex items-center basis-11/12">
            <img
               src={ipfsPath(props.data.img)}
               alt={props.data.name}
               className="rounded-full h-20 aspect-square object-contain m-auto mx-2"
            />
            <div className="flex flex-col gap-0.5">
               <Heading>{props.data.name}</Heading>
               <Paragraph className="text-neutral-400 line-clamp-2">
                  {props.data.metadata.description}
               </Paragraph>
            </div>
         </div>
         <div>
            {!props.data.unresolved.length ? (
               <CheckmarkIcon className="fill-white" />
            ) : (
               <span className="text-amber-300">{props.data.unresolved.length} missed</span>
            )}
         </div>
      </components.Option>
   )
}
