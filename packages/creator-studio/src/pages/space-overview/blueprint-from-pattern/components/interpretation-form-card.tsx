import { FormikProps } from 'formik'
import { useQuery } from 'react-query'

import { InterpretationPattern } from '@asylum-ui/connection-library'

import { fetchTags } from 'api'
import { Card } from 'components/card'
import { HeadingLg, Paragraph } from 'components/text'
import { InterpretationForm, InterpretationFormValues } from 'modules/form/interpretation-form'
import {
   buildInterpretationInitialValues,
   defineInterpretationType,
} from 'utils/interpretationHelpers'

const createInterpretationHeader = (interpretation: InterpretationPattern) => {
   if (interpretation.tags?.includes('default-view')) return 'Default'

   switch (defineInterpretationType(interpretation)) {
      case '3d-model':
         return '3d-model'
      case 'image':
         return 'Image'
      default:
         return 'Unspecified'
   }
}

interface InterpretationFormCardProps {
   interpretation: InterpretationPattern
   setFormik: (formik: FormikProps<InterpretationFormValues>) => void
}

export const InterpretationFormCard = ({
   interpretation,
   setFormik,
}: InterpretationFormCardProps) => {
   const { data: tags = [] } = useQuery('tags', fetchTags)

   const initialValues = buildInterpretationInitialValues(interpretation, tags)
   return (
      <Card header={<Header interpretation={interpretation} />} headerClassName="py-4">
         <InterpretationForm
            setFormik={setFormik}
            initialValues={initialValues}
            lockedValues={interpretation}
         />
      </Card>
   )
}

interface HeaderProps {
   interpretation: InterpretationPattern
}

const Header = ({ interpretation }: HeaderProps) => (
   <div className="flex items-end gap-4">
      <HeadingLg>{`${
         interpretation.name ?? createInterpretationHeader(interpretation)
      } interpretation`}</HeadingLg>
      {interpretation.description && (
         <>
            <span>—</span>
            <Paragraph className="font-secondary leading-0.5">
               {interpretation.description}
            </Paragraph>
         </>
      )}
   </div>
)
