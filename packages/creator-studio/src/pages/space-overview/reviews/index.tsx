import { Review } from '@asylum-ui/connection-library'

import { Card } from 'components/card'
import { ReviewCard } from 'components/review-item'
import { HeadingXl, Paragraph } from 'components/text'
import { ComponentProps } from 'types'

interface IReview extends ComponentProps {
   reviews: Review[]
}

export const Reviews = ({ reviews }: IReview) => {
   return (
      <>
         <HeadingXl className="text-white my-3">Reviews</HeadingXl>
         {reviews.map((review) => (
            <ReviewCard key={review.id} review={review} />
         ))}
         {reviews.length === 0 && (
            <Card>
               <Paragraph>No reviews yet.</Paragraph>
            </Card>
         )}
      </>
   )
}
