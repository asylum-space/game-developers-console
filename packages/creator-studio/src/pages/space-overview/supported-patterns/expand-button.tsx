import { Button } from 'components/button'
import { BlueprintUnwrapped } from 'types'
import { ipfsPath } from 'utils'

interface ExpandButtonProps {
   setExpanded: () => void
   blueprints: BlueprintUnwrapped[]
}

export const ExpandButton = ({ setExpanded, blueprints }: ExpandButtonProps) => {
   return (
      <Button
         onClick={setExpanded}
         className="basis-5/12 p-0 flex justify-between items-center self-end rounded-lg rounded-l-[30px] bg-white h-12"
      >
         <div className="flex">
            {blueprints.slice(0, 3).map((blueprint) => (
               <img
                  key={blueprint.id}
                  alt={blueprint.name}
                  src={ipfsPath(blueprint.img)}
                  className="rounded-full bg-radial-gradient-grayish p-0.5 h-12 first:ml-0 -ml-4 object-contain aspect-square"
               />
            ))}
         </div>
         <span className="mr-8">show more</span>
      </Button>
   )
}
