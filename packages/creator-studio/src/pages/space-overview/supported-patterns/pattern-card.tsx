import { useQuery } from 'react-query'
import { Link } from 'react-router-dom'

import { Pattern } from '@asylum-ui/connection-library'

import { fetchBlueprints } from 'api'
import { Button } from 'components/button'
import { Card } from 'components/card'
import { HeadingLg, Paragraph } from 'components/text'
import { BlueprintList } from 'pages/space-overview/supported-patterns/blueprint-list'
import { ExpandButton } from 'pages/space-overview/supported-patterns/expand-button'
import { TagGroupList } from 'pages/space-overview/supported-patterns/tag-group-list'
import { filterMatchedPattern, ipfsPath } from 'utils'

interface PatternCardProps {
   pattern: Pattern
   expanded: boolean
   setExpanded: () => void
}

export const PatternCard = ({ pattern, setExpanded }: PatternCardProps) => {
   const { data: blueprints } = useQuery('blueprints', fetchBlueprints)

   // TODO: Replace with inherited expanded prop for collapse logic
   const expanded = true

   const matchedBlueprints = blueprints ? filterMatchedPattern(pattern, blueprints) : []

   const blueprintFromPatternURL = `pattern/${pattern.name}`

   return (
      <Card
         header={
            <>
               <HeadingLg>{pattern.name}</HeadingLg>
               <div className="flex gap-5">
                  <Link to={`${blueprintFromPatternURL}?extend=true`}>
                     <Button>adapt existing</Button>
                  </Link>
                  <Link to={blueprintFromPatternURL}>
                     <Button>create new</Button>
                  </Link>
               </div>
            </>
         }
         headerClassName="flex justify-between items-center pl-9 py-5"
         contentClassName="flex justify-between items-stretch gap-10"
         dataTestId="pattern-card"
      >
         <>
            <div className="basis-7/12 flex flex-col gap-4">
               <TagGroupList interpretations={pattern.interpretations} />
               {expanded && (
                  <div className="flex flex-col gap-2">
                     <Paragraph className="text-lg p-1">{pattern.description}</Paragraph>
                     <img
                        alt={pattern.name}
                        src={ipfsPath(pattern.img)}
                        className="aspect-video object-cover bg-gradient-grayish rounded-3xl"
                     />
                  </div>
               )}
            </div>
            {expanded ? (
               <BlueprintList blueprints={matchedBlueprints} />
            ) : (
               <ExpandButton setExpanded={setExpanded} blueprints={matchedBlueprints} />
            )}
         </>
      </Card>
   )
}
