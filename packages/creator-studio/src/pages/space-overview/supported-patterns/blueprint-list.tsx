import classNames from 'classnames'
import { Link } from 'react-router-dom'

import { Heading, HeadingLg, Paragraph } from 'components/text'
import { BlueprintUnwrapped } from 'types'
import { ipfsPath } from 'utils'

interface BlueprintListProps {
   blueprints: BlueprintUnwrapped[]
}

export const BlueprintList = ({ blueprints }: BlueprintListProps) => {
   return (
      <div
         className={classNames('basis-5/12 flex flex-col gap-5', {
            'justify-center': !blueprints.length,
         })}
      >
         {blueprints.length ? (
            <>
               <Heading className="p-3 text-[1.25rem] leading-5">Associated blueprints:</Heading>
               {blueprints.map((blueprint) => (
                  <Link
                     to={`/blueprints/${blueprint.id}`}
                     key={blueprint.id}
                     className="rounded-r-[40px] rounded-tl-3xl rounded-bl-none border-glitch-effect transition-all"
                  >
                     <div className="flex justify-between h-16">
                        <div className="m-3 basis-3/4 overflow-hidden">
                           <HeadingLg className="leading-5 text-inherit">
                              {blueprint.name}
                           </HeadingLg>
                           <Paragraph>{blueprint.metadata.description}</Paragraph>
                        </div>
                        <img
                           key={blueprint.id}
                           alt={blueprint.name}
                           src={ipfsPath(blueprint.img)}
                           className="rounded-full bg-radial-gradient-grayish h-full p-0.5 object-contain aspect-square"
                        />
                     </div>
                  </Link>
               ))}
            </>
         ) : (
            <HeadingLg className="self-center text-gray-700">No matched blueprints found</HeadingLg>
         )}
      </div>
   )
}
