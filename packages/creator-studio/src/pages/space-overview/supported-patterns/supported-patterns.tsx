import { useState } from 'react'

import { useQuery } from 'react-query'

import { Pattern } from '@asylum-ui/connection-library'

import { fetchBlueprints } from 'api'
import { HeadingXl } from 'components/text'

import { PatternCard } from './pattern-card'

interface supportedPatternsProps {
   patterns: Pattern[]
}

export const SupportedPatterns = ({ patterns }: supportedPatternsProps) => {
   const { data: blueprints } = useQuery('blueprints', fetchBlueprints)

   const [expandedPattern, setExpandedPattern] = useState<number | null>(null)

   if (!blueprints) return null

   return (
      <>
         <HeadingXl className="text-white my-3">Supported Patterns</HeadingXl>
         {patterns.map((pattern, index) => (
            <PatternCard
               key={index}
               pattern={pattern}
               expanded={index === expandedPattern}
               setExpanded={() => setExpandedPattern(index)}
            />
         ))}
      </>
   )
}
