import React from 'react'

import { observer } from 'mobx-react-lite'

import { AsylumApi, IAsylumApi } from '@asylum-ui/connection-library'

import { ComponentProps } from 'types'

const apiProvider = React.createContext<IAsylumApi | null>(null)

export const AsylumApiProvider = observer(({ children }: ComponentProps) => {
   return <apiProvider.Provider value={AsylumApi}>{children}</apiProvider.Provider>
})
