import { createContext } from 'react'

export type colorVariants = 'black' | 'gray'

export const ColorContext = createContext<colorVariants>('black')
