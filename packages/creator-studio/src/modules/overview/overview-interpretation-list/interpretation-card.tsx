import { useContext, useEffect, useRef, useState } from 'react'

import classNames from 'classnames'

import { ReactComponent as ArrowDown } from 'assets/svg/arrow-down.svg'
import { ReactComponent as EditIcon } from 'assets/svg/pen.svg'
import { Card } from 'components/card'
import { FullscreenPrompt } from 'components/fullscreen-prompt'
import { JsonRaw } from 'components/json-raw'
import { TagGroup } from 'components/tag'
import { GalleryStateContext } from 'modules/overview/overview'
import { InterpretationRepresentation } from 'modules/overview/overview-card/interpretation-representation'
import { CarouselSupportedTypes } from 'modules/overview/overview-card/representation-carousel'
import { ComponentProps, InterpretationUnwrapped } from 'types'
import { defineInterpretationType } from 'utils/interpretationHelpers'

interface InterpretationCardProps {
   interpretation: InterpretationUnwrapped
   setIsEditInterpretationModalOpen?: (isOpen: boolean) => void
   setInterpretationEdited?: (interpretation: InterpretationUnwrapped) => void
}

const isOverflown = (elem: HTMLDivElement) => {
   return elem.scrollHeight - elem.clientHeight > 50
}

export const InterpretationCard = ({
   interpretation,
   setIsEditInterpretationModalOpen,
   setInterpretationEdited,
}: ComponentProps<InterpretationCardProps>) => {
   const setGalleryOpen = useContext(GalleryStateContext)?.[1]
   const [collapse, setCollapse] = useState<'needless' | 'collapsed' | 'expanded' | 'unknown'>(
      'unknown'
   )

   const preRef = useRef(null)

   const supportedByCarousel = CarouselSupportedTypes.includes(
      defineInterpretationType(interpretation)
   )

   useEffect(() => {
      if (!preRef.current) return
      if (isOverflown(preRef.current)) {
         setCollapse('collapsed')
      } else {
         setCollapse('needless')
      }
   }, [preRef.current])

   const handleEditInterpretation = () => {
      if (!setIsEditInterpretationModalOpen || !setInterpretationEdited) return

      setInterpretationEdited(interpretation)
      setIsEditInterpretationModalOpen(true)
   }

   const handleInterpretationClick = () => {
      if (!setGalleryOpen || !supportedByCarousel) {
         return
      }
      document.getElementById(`${interpretation.id}-thumb`)?.click()
      setGalleryOpen(true)
   }

   return (
      <>
         <Card contentClassName="p-7">
            <div className="flex items-start gap-8 relative">
               <div className="w-40 shrink-0 relative">
                  <InterpretationRepresentation
                     isPending={interpretation.pending}
                     interpretation={interpretation}
                     className="h-full"
                  />
                  {supportedByCarousel && <FullscreenPrompt onClick={handleInterpretationClick} />}
               </div>
               <div
                  className={classNames(
                     'relative grow flex flex-col gap-5 overflow-y-hidden',
                     collapse === 'expanded' && 'mb-4'
                  )}
               >
                  <div className="flex justify-between items-center">
                     <TagGroup tags={interpretation.tags} />
                     {setIsEditInterpretationModalOpen && setInterpretationEdited && (
                        <button
                           onClick={handleEditInterpretation}
                           className="group cursor-pointer p-2"
                        >
                           <EditIcon className="fill-neutral-500 group-hover:fill-white transition-colors" />
                        </button>
                     )}
                  </div>
                  <JsonRaw
                     ref={preRef}
                     metadata={interpretation.interpretationInfo.metadata}
                     className={classNames(
                        collapse === 'collapsed' || collapse === 'unknown'
                           ? 'max-h-32 overflow-hidden'
                           : 'max-h-[none]'
                     )}
                  />
               </div>
               {collapse !== 'needless' && (
                  <button
                     onClick={() => setCollapse(collapse === 'expanded' ? 'collapsed' : 'expanded')}
                     className={classNames(
                        'group absolute right-0 -bottom-6  h-16 w-[calc(100%_-_12rem)]  flex justify-center items-center cursor-pointer',
                        collapse === 'collapsed' && 'bg-gradient-to-transparency'
                     )}
                  >
                     <ArrowDown
                        className={classNames(
                           'fill-neutral-500 group-hover:fill-white transition-colors',
                           collapse === 'expanded' && 'rotate-180'
                        )}
                     />
                  </button>
               )}
            </div>
         </Card>
      </>
   )
}
