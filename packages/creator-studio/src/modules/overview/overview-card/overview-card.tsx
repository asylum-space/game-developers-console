import { Ref, useRef, useState } from 'react'

import classNames from 'classnames'
import { observer } from 'mobx-react-lite'
import { useQuery } from 'react-query'

import { fetchSpaces } from 'api'
import empty from 'assets/svg/empty.svg'
import { Card } from 'components/card'
import { Heading, HeadingLg, Paragraph } from 'components/text'
import {
   CarouselSupportedTypes,
   RepresentationCarousel,
} from 'modules/overview/overview-card/representation-carousel'
import { SpacesList } from 'modules/overview/overview-card/spaces-list'
import { useStore } from 'store'
import { BlueprintUnwrapped, ItemUnwrapped } from 'types'
import { formatAddress, isMatchingPattern } from 'utils'
import { compareByTags, defineInterpretationType } from 'utils/interpretationHelpers'

interface OverviewCardProps {
   data: BlueprintUnwrapped | ItemUnwrapped
}

export const OverviewCard = observer(({ data }: OverviewCardProps) => {
   const store = useStore()

   const { data: spaces = [] } = useQuery(['spaces', store.account?.address], () =>
      store.account ? fetchSpaces(store.account) : []
   )

   const [seeMore, setSeeMore] = useState(false)
   const descriptionRef: Ref<HTMLDivElement> = useRef(null)
   const isDescriptionClamped = descriptionRef.current
      ? descriptionRef.current.scrollHeight > descriptionRef.current.clientHeight
      : false

   const matchingSpaces = spaces.filter((spaces) =>
      spaces.patterns?.some((pattern) => isMatchingPattern(pattern, data))
   )

   const visualInterpretations = data.interpretations.filter((interpretation) =>
      CarouselSupportedTypes.includes(defineInterpretationType(interpretation))
   )

   return (
      <Card
         header={
            <>
               <div className="flex justify-between pr-14 basis-7/12">
                  <HeadingLg className="flex gap-3 items-center">
                     {'name' in data ? data.name : data.metadata.name}
                  </HeadingLg>
                  <div className="font-secondary">
                     ID:{' '}
                     <span className="font-light">
                        {'blueprintId' in data ? [data.id, data.blueprintId].join('_') : data.id}
                     </span>
                  </div>
               </div>
               <div className="flex font-secondary gap-2 basis-5/12">
                  {'issuer' in data ? (
                     <>
                        Issuer:
                        <span className="font-light">{formatAddress(data.issuer || '')}</span>
                     </>
                  ) : (
                     <>
                        Owner:
                        <span className="font-light">
                           {formatAddress(data.owner.AccountId || '')}
                        </span>
                     </>
                  )}
               </div>
            </>
         }
         headerClassName="flex justify-between gap-9 px-9"
         contentClassName="flex justify-between gap-9 pt-8"
      >
         <>
            {visualInterpretations.length ? (
               <RepresentationCarousel
                  className="basis-7/12"
                  interpretations={visualInterpretations.sort(compareByTags)}
               />
            ) : (
               <img src={empty} alt="empty" className="basis-7/12 aspect-video" />
            )}

            <div className="flex flex-col basis-5/12 gap-8">
               {!!matchingSpaces?.length && <SpacesList spaces={matchingSpaces} />}
               <div>
                  <Heading className="mb-3">Description</Heading>
                  <div ref={descriptionRef} className={classNames({ 'line-clamp-9': !seeMore })}>
                     {data.metadata.description.split('\n').map((paragraph, index) => (
                        <Paragraph key={index} className="mb-3">
                           {paragraph}
                        </Paragraph>
                     ))}
                  </div>
                  {isDescriptionClamped && (
                     <a
                        className={classNames(
                           'cursor-pointer underline text-base text-asylum-blue',
                           {
                              hidden: seeMore,
                           }
                        )}
                        onClick={() => setSeeMore(true)}
                     >
                        See more
                     </a>
                  )}
               </div>
            </div>
         </>
      </Card>
   )
})
