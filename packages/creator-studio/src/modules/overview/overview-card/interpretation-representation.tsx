import classNames from 'classnames'
import { twMerge } from 'tailwind-merge'

import empty from 'assets/svg/empty.svg'
import { ThreeViewer } from 'components/three-viewer'
import { ComponentProps, InterpretationUnwrapped } from 'types'
import { ipfsPath } from 'utils'
import { InterpretationViewType, defineInterpretationType } from 'utils/interpretationHelpers'

interface InterpretationRepresentationProps {
   interpretation: InterpretationUnwrapped
   isPending?: boolean
}

const constructView = (interpretation: InterpretationUnwrapped, type: InterpretationViewType) => {
   switch (type) {
      case 'image':
         return (
            <img
               src={ipfsPath(interpretation.interpretationInfo.src)}
               alt={interpretation.id}
               className="object-contain h-full m-auto rounded"
            />
         )
      case '3d-model':
         return (
            <ThreeViewer
               src={ipfsPath(interpretation.interpretationInfo.src as string)}
               alt={interpretation.id}
               auto-rotate={true}
               rotation-per-second="15deg"
               className="object-contain m-auto rounded"
            />
         )
      case 'empty':
      case 'unknown':
      default:
         return (
            <img
               src={empty}
               alt={interpretation.id}
               className="h-full object-contain m-auto rounded"
            />
         )
   }
}

export const InterpretationRepresentation = ({
   interpretation,
   isPending = false,
   className,
}: ComponentProps<InterpretationRepresentationProps>) => {
   return (
      <div className={twMerge(classNames('relative'), className)}>
         {isPending && (
            <span className="bg-yellow-400 py-1 px-3 rounded-br-lg text-white absolute left-0 top-0 text-sm z-10">
               pending approval
            </span>
         )}
         {constructView(interpretation, defineInterpretationType(interpretation))}
      </div>
   )
}
