import { observer } from 'mobx-react-lite'
import { useNavigate } from 'react-router-dom'

import { Button } from 'components/button'
import { Heading } from 'components/text'
import { useStore } from 'store'
import { SpaceWithMetadataUnwrapped } from 'types'

interface Props {
   spaces: SpaceWithMetadataUnwrapped[]
}

export const SpacesList = observer(({ spaces }: Props) => {
   const store = useStore()
   const navigate = useNavigate()

   const handleClick = (space: SpaceWithMetadataUnwrapped) => {
      store.setSelectedSpace(space)
      navigate('/overview')
   }

   return (
      <div>
         <Heading className="mb-4">Supported by:</Heading>
         <div className="flex gap-2.5 flex-wrap">
            {spaces.map((space) => (
               <Button key={space.id} onClick={() => handleClick(space)} className="px-2 py-0.5">
                  {space.title}
               </Button>
            ))}
         </div>
      </div>
   )
})
