import { useState } from 'react'

import { observer } from 'mobx-react-lite'

import { AsylumApi } from '@asylum-ui/connection-library'

import { Keyring } from '@polkadot/api'
import { InjectedAccountWithMeta } from '@polkadot/extension-inject/types'
import { Avatar } from 'components/avatar'
import { Button } from 'components/button'
import { useStore } from 'store'
import { formatAddress } from 'utils'

import { WalletConnectModal } from './wallet-connect-modal'

export const WalletConnect = observer(() => {
   const store = useStore()
   const [isModalOpen, setIsModalOpen] = useState(false)

   const onConnectWallet = async () => {
      if (process.env.REACT_APP_NODE_ENV === 'test') {
         if (!store.account) {
            const tester = new Keyring({ type: 'sr25519' }).addFromUri(
               process.env.REACT_APP_MINTER_MNEMONIC || ''
            )
            await AsylumApi.withKeyringPair(tester)
            store.setSelectedSpace(null)
            // @ts-ignore
            store.setAccount(tester as InjectedAccountWithMeta)
         }
      } else {
         setIsModalOpen(!isModalOpen)
      }
   }
   console.log(process.env.REACT_APP_NODE_ENV)

   return (
      <>
         {store.account ? (
            <div className="flex gap-4 items-center">
               <Avatar />
               <Button variant="dark" onClick={onConnectWallet}>
                  {formatAddress(store.account.address)}
               </Button>
            </div>
         ) : (
            <Button onClick={onConnectWallet}>connect wallet</Button>
         )}

         <WalletConnectModal open={isModalOpen} onClose={() => setIsModalOpen(false)} />
      </>
   )
})
