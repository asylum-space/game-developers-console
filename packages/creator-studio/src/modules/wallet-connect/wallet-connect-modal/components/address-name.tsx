import { Avatar } from 'components/avatar'
import { ButtonWithIcon } from 'components/button'

interface IAddressName {
   name?: string
}

export const AddressName = ({ name }: IAddressName) => {
   return (
      <ButtonWithIcon icon={<Avatar />} disabled={true} className="text-neutral-300">
         {name}
      </ButtonWithIcon>
   )
}
