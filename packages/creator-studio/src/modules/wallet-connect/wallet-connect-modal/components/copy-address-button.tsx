import { useState } from 'react'

import classNames from 'classnames'

import { ReactComponent as CheckmarkIcon } from 'assets/svg/checkmark.svg'
import { ReactComponent as CopyIcon } from 'assets/svg/copy.svg'
import { Button } from 'components/button'
import { formatAddress } from 'utils'

interface ICopyAddressButton {
   address: string
}

export const CopyAddressButton = ({ address }: ICopyAddressButton) => {
   const [copied, setCopied] = useState(false)

   return (
      <Button
         variant={copied ? 'success' : 'dark'}
         className="flex gap-3 items-center"
         onClick={() => {
            navigator.clipboard.writeText(address)
            setCopied(true)
         }}
      >
         {copied ? (
            <CheckmarkIcon className="fill-white" />
         ) : (
            <CopyIcon
               className={classNames('fill-neutral-500 duration-100', {
                  'group-hover:fill-white': !copied,
               })}
            />
         )}
         {copied ? 'Address copied' : formatAddress(address)}
      </Button>
   )
}
