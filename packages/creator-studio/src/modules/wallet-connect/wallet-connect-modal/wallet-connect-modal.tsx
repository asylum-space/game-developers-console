import { useState } from 'react'

import { observer } from 'mobx-react-lite'

import { Modal } from 'components/modal'
import { useStore } from 'store'

import { WALLET_CONNECT_STEPS, WalletConnectStepType } from './steps'

interface IWalletConnectModal {
   open?: boolean
   onClose?: () => void
}

export const WalletConnectModal = observer(({ open, onClose }: IWalletConnectModal) => {
   const store = useStore()
   const [step, nextStep] = useState<WalletConnectStepType>(
      store.account ? WalletConnectStepType.Connected : WalletConnectStepType.ExtensionSelection
   )

   const handleClose = () => {
      onClose && onClose()
      if (!store.account) {
         setTimeout(() => nextStep(WalletConnectStepType.ExtensionSelection), 500)
      }
   }

   return (
      <Modal
         open={open}
         title={WALLET_CONNECT_STEPS[step].name}
         maxWidth="lg"
         onClose={handleClose}
      >
         <div className="p-4">
            {WALLET_CONNECT_STEPS[step].component({
               nextStep,
               onClose: handleClose,
            })}
         </div>
      </Modal>
   )
})
