import { useContext, useState } from 'react'

import classNames from 'classnames'
import { FormikProps } from 'formik'
import { useQueryClient } from 'react-query'
import { toast } from 'react-toastify'

import { AsylumApi, BlueprintChangeAdd, InterpretationCreate } from '@asylum-ui/connection-library'

import { ReactComponent as PlusIcon } from 'assets/svg/plus.svg'
import { Button } from 'components/button'
import { Modal } from 'components/modal'
import { ColorContext } from 'context'
import { InterpretationForm, InterpretationFormValues } from 'modules/form/interpretation-form'
import { BlueprintUnwrapped } from 'types'
import { touchAndValidate } from 'utils'

interface IAddInterpretationModal {
   blueprint: BlueprintUnwrapped
   open: boolean
   onClose: () => void
}

export const AddInterpretationModal = ({ blueprint, open, onClose }: IAddInterpretationModal) => {
   const queryClient = useQueryClient()
   const colorContextValue = useContext(ColorContext)

   const [interpretationFormik, setInterpretationFormik] =
      useState<FormikProps<InterpretationFormValues> | null>(null)

   const [isSubmitting, setIsSubmitting] = useState(false)

   const submitHandler = async () => {
      if (!interpretationFormik) return
      await touchAndValidate(interpretationFormik)
      if (!interpretationFormik.isValid) return
      try {
         setIsSubmitting(true)
         const interpretation = (await interpretationFormik.submitForm()) as
            | InterpretationCreate
            | undefined
         if (!interpretation) return

         const changeSet = [new BlueprintChangeAdd([interpretation])]
         const blueprintId = parseInt(blueprint.id)

         await AsylumApi.updateBlueprint(blueprintId, changeSet)
         toast.success('Interpretation created!')
         queryClient.invalidateQueries(['blueprints', blueprint.id])

         onClose && onClose()
      } catch (error) {
         toast.error(typeof error === 'string' ? error : 'Unknown Error')
      } finally {
         setIsSubmitting(false)
      }
   }

   return (
      <Modal
         open={open}
         onClose={onClose}
         title="Add interpretation"
         className="text-white"
         maxWidth="2xl"
      >
         <div className="flex flex-col gap-7 p-4">
            <InterpretationForm setFormik={setInterpretationFormik} />
            <Button
               variant="light"
               className={classNames({ 'animate-pulse': isSubmitting })}
               onClick={submitHandler}
               disabled={isSubmitting}
               surroundColor={colorContextValue}
            >
               {isSubmitting ? (
                  'Submitting'
               ) : (
                  <>
                     <PlusIcon className="fill-text-base w-4 h-4 inline-block mr-2" /> add
                     interpretation
                  </>
               )}
            </Button>
         </div>
      </Modal>
   )
}
