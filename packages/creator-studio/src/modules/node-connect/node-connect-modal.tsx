import { Modal } from 'components/modal'
import { ComponentProps } from 'types'

import { ConnectForm } from '../form/connect-form'

interface ServerConnectModalProps {
   open: boolean
   onClose?: () => void
}

export const NodeConnectModal = ({ open, onClose }: ComponentProps<ServerConnectModalProps>) => {
   return (
      <Modal open={open} onClose={onClose} title="Connect to Node">
         <ConnectForm onClose={onClose} />
      </Modal>
   )
}
