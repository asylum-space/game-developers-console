import { useEffect } from 'react'

import classNames from 'classnames'
import { FormikErrors, FormikProps, useFormik } from 'formik'
import { observer } from 'mobx-react-lite'
import { twMerge } from 'tailwind-merge'

import { AsylumApi, BlueprintCreate } from '@asylum-ui/connection-library'

import { InputField } from 'components/input'
import { useStore } from 'store'
import { ComponentProps } from 'types'
import { formatAddress } from 'utils'

export interface BlueprintFormValues {
   name: string
   description: string
}

interface Props {
   setFormik: (formik: FormikProps<BlueprintFormValues>) => void
   initialValues?: BlueprintFormValues
}

const defaultValues: BlueprintFormValues = {
   name: '',
   description: '',
}

export const BlueprintForm = observer(
   ({ setFormik, initialValues, className }: ComponentProps<Props>) => {
      const store = useStore()

      const formik = useFormik<BlueprintFormValues>({
         initialValues: initialValues || defaultValues,
         validate: (values: BlueprintFormValues) => {
            const errors: FormikErrors<BlueprintFormValues> = {}

            if (!values.name) {
               errors.name = 'Name is required'
            }

            if (!values.description) {
               errors.description = 'Description is required'
            }

            return errors
         },
         onSubmit: async (values, { setSubmitting }) => {
            try {
               const MetadataCID = await AsylumApi.uploadMetadata({
                  description: values.description,
               })
               return {
                  name: values.name,
                  metadata: MetadataCID,
               } as BlueprintCreate
            } catch (error) {
               console.error(error)
            } finally {
               setSubmitting(false)
            }
         },
         validateOnMount: true,
         enableReinitialize: true,
      })

      useEffect(() => {
         setFormik && setFormik(formik)
      }, [formik.values, formik.isSubmitting, formik.isValid])

      return (
         <form className={twMerge(classNames('flex flex-col gap-4'), className)}>
            <div className="flex gap-4">
               <InputField
                  as="input"
                  className="basis-8/12"
                  label="Name"
                  placeholder="Name"
                  name="name"
                  value={formik.values.name}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  errorMessage={formik.touched.name && formik.errors.name}
               />
               <InputField
                  as="input"
                  className="basis-4/12"
                  defaultValue={formatAddress(store.account?.address || '')}
                  label="Issuer"
                  placeholder="Issuer Address"
                  name="issuer"
                  readOnly
                  disabled
               />
            </div>
            <InputField
               as="textarea"
               label="Description"
               placeholder="Description"
               name="description"
               rows={3}
               value={formik.values.description}
               onChange={formik.handleChange}
               onBlur={formik.handleBlur}
               errorMessage={formik.touched.description && formik.errors.description}
            />
         </form>
      )
   }
)
