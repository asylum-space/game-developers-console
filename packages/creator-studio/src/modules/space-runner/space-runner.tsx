import { useEffect, useState } from 'react'

import { web3FromAddress } from '@polkadot/extension-dapp'
import { observer } from 'mobx-react-lite'
import Unity, { UnityContext } from 'react-unity-webgl'

import { AsylumApi, Item, getEventByName } from '@asylum-ui/connection-library'

import { Keyring } from '@polkadot/api'
import { fetchBlueprints } from 'api'
import { ReactComponent as ArrowUpIcon } from 'assets/svg/arrow-up.svg'
import { Button } from 'components/button'
import { HeadingXl } from 'components/text'
import { useStore } from 'store'
import { filterMatchedSomePatterns, ipfsPath, isMatchingSomePatterns } from 'utils'

export const SpaceRunner = observer(() => {
   const store = useStore()
   const [loaded, setLoaded] = useState(false)
   const [loadedProgress, setLoadedProgress] = useState(0)

   async function extendItemsWithInterpretations(items: Item[]) {
      return Promise.all(
         items.map(async (item) => ({
            ...item,
            interpretations: await AsylumApi.itemInterpretations(item.blueprintId, item.id),
         }))
      )
   }

   async function sendUserItems() {
      const items = await AsylumApi.itemsByOwner(store.account?.address || '')
      const blueprints = await fetchBlueprints()
      const patterns = store.selectedSpace?.patterns
      const spaceBlueprintIds = filterMatchedSomePatterns(patterns ?? [], blueprints).map(
         (blueprint) => blueprint.id
      )
      const filteredItems = await extendItemsWithInterpretations(
         items.filter((item) => spaceBlueprintIds.includes(item.blueprintId))
      )

      const unityMessage = '{ "Items" :' + JSON.stringify(filteredItems) + '}'
      store.unityContext.send('ReactController', 'ParseItems', unityMessage)
   }

   async function parseUserInfo() {
      const unityMessage =
         '{ "User" :' +
         JSON.stringify({
            name: store?.account?.meta.name,
            address: store?.account?.address,
         }) +
         '}'
      store.unityContext.send('ReactController', 'ParseUserInfo', unityMessage)
   }

   async function sendItemsMinted(items: Item[]) {
      const itemsWithInterpretations = await extendItemsWithInterpretations(items)

      const unityMessage = '{ "Items" :' + JSON.stringify(itemsWithInterpretations) + '}'
      store.unityContext.send('ReactController', 'ItemsMinted', unityMessage)
   }

   async function sendBlueprints() {
      const patterns = store.selectedSpace?.patterns
      const blueprints = await fetchBlueprints()
      const BlueprintsBySpace = patterns
         ? blueprints.filter((blueprint) => isMatchingSomePatterns(patterns, blueprint))
         : []
      // hack to parse array of objects in Unity
      const unityMessage = '{ "Items" :' + JSON.stringify(BlueprintsBySpace) + '}'
      store.unityContext.send('ReactController', 'ParseBlueprints', unityMessage)
   }

   async function sendBlueprint(blueprintId: string) {
      const blueprints = await AsylumApi.blueprint(blueprintId)
      store.unityContext.send('ReactController', 'ParseBlueprint', JSON.stringify(blueprints))
   }

   async function sendSpaceMetadata() {
      const spaceMetadata = await AsylumApi.spaceMetadataOf(8)
      store.unityContext.send(
         'ReactController',
         'ParseSpaceMetadata',
         JSON.stringify(spaceMetadata)
      )
   }

   async function sendBlueprintInterpretations(blueprintId: string) {
      const interpretations = await AsylumApi.blueprintInterpretations(blueprintId)
      // hack to parse array of objects in Unity
      const unityMessage = '{ "Items" :' + JSON.stringify(interpretations) + '}'
      store.unityContext.send('ReactController', 'ParseInterpretations', unityMessage)
   }

   async function sendItemInterpretations(blueprintId: string, itemId: string) {
      const interpretations = await AsylumApi.itemInterpretations(blueprintId, itemId)
      // hack to parse array of objects in Unity
      const unityMessage = '{ "Items" :' + JSON.stringify(interpretations) + '}'
      store.unityContext.send('ReactController', 'ParseInterpretations', unityMessage)
   }

   async function sendPauseSpace() {
      store.unityContext.send('ReactController', 'PauseSpace')
   }

   useEffect(() => {
      if (store.isSpaceRunning && store.isInterfaceOpen) {
         sendPauseSpace()
      }
   }, [store.isInterfaceOpen])

   useEffect(() => {
      if (store.isSpaceRunning) {
         setTimeout(() => {
            const spaceClient = store.selectedSpace?.spaceClient

            store.setUnityContext(
               new UnityContext({
                  productName: store.selectedSpace?.title,
                  companyName: 'Asylum',
                  loaderUrl: ipfsPath(spaceClient?.loader),
                  dataUrl: ipfsPath(spaceClient?.data),
                  frameworkUrl: ipfsPath(spaceClient?.framework),
                  codeUrl: ipfsPath(spaceClient?.wasm),
                  webglContextAttributes: {
                     preserveDrawingBuffer: true,
                  },
               })
            )
            store.unityContext.on('progress', (progress: any) => {
               setLoadedProgress(progress)
            })
            store.unityContext.on('loaded', () => {
               setLoaded(true)
            })
         }, 500)
      } else {
         setTimeout(() => {
            if (store.unityContext) {
               store.unityContext.removeAllEventListeners()
            }
            store.setUnityContext(null)
            setLoadedProgress(0)
            setLoaded(false)
         }, 500)
      }
   }, [store.isSpaceRunning])

   useEffect(() => {
      if (loaded) {
         store.unityContext.on('Blueprints', async () => {
            sendBlueprints()
         })
         store.unityContext.on('UserItems', async () => {
            sendUserItems()
         })
         store.unityContext.on('UserInfo', async () => {
            parseUserInfo()
         })
         store.unityContext.on('SpaceMeta', async () => {
            sendSpaceMetadata()
         })
         store.unityContext.on('BlueprintByID', async (blueprintId: string) => {
            sendBlueprint(blueprintId)
         })
         store.unityContext.on('InterpretationsByBlueprintID', async (blueprintId: string) => {
            sendBlueprintInterpretations(blueprintId)
         })
         store.unityContext.on(
            'InterpretationsByItemID',
            async (blueprintId: string, itemId: string) => {
               sendItemInterpretations(blueprintId, itemId)
            }
         )
         store.unityContext.on('SpaceClose', async () => {
            await store.unityContext.quitUnityInstance()
            store.setIsSpaceRunning(false)
         })

         store.unityContext.on(
            'RequestMintItem',
            async (blueprintId: string, name: string, description: string) => {
               const metadataCID = await AsylumApi.uploadMetadata({
                  name,
                  description,
               })
               const minter = new Keyring({ type: 'sr25519' }).addFromUri(
                  process.env.REACT_APP_MINTER_MNEMONIC || ''
               )

               const result = await AsylumApi.withKeyringPair(minter).mintItem(
                  store.account!.address,
                  blueprintId,
                  metadataCID
               )

               const injector = await web3FromAddress(store!.account!.address)
               AsylumApi.withInjectedSigner(store!.account!.address, injector.signer)

               const {
                  data: [, itemId],
               } = getEventByName(result, 'ItemMinted')

               const item = await AsylumApi.item(blueprintId, itemId)
               await sendItemsMinted([item])
            }
         )
      }
   }, [loaded])

   return (
      <div className="absolute min-h-screen inset-0 z-0">
         {!loaded && Boolean(loadedProgress) && (
            <HeadingXl className="absolute-center text-white">
               Downloading Space Client: {Math.floor(loadedProgress * 100)}%
            </HeadingXl>
         )}
         {store.unityContext && (
            <Unity className="absolute inset-0 w-full h-full" unityContext={store.unityContext} />
         )}
         <Button
            className="!absolute bottom-0 left-0 opacity-50 hover:opacity-100 !p-2"
            onClick={() => store.setIsInterfaceOpen(true)}
         >
            <ArrowUpIcon className="group-hover:fill-white" />
         </Button>
      </div>
   )
})
