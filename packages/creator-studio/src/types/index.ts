import React, { ElementType } from 'react'

import {
   Blueprint,
   Interpretation,
   InterpretationInfo,
   Item,
   Pattern,
   Review,
   Space,
} from '@asylum-ui/connection-library'

export interface InterpretationInfoUnwrapped extends Omit<InterpretationInfo, 'metadata'> {
   metadata: any
}

export interface BlueprintUnwrapped extends Omit<Blueprint, 'metadata'> {
   interpretations: InterpretationUnwrapped[]
   metadata: {
      description: string
   }
   img: string
   defaultInterpretation: InterpretationUnwrapped
}

export interface InterpretationUnwrapped extends Interpretation {
   interpretationInfo: InterpretationInfoUnwrapped
}

export interface ItemUnwrapped extends Omit<Item, 'metadata'> {
   interpretations: InterpretationUnwrapped[]
   metadata: {
      name: string
      description: string
   }
   img: string
   defaultInterpretation: InterpretationUnwrapped
   hasPendingInterpretations: boolean
}

export interface SpaceMetadataUnwrapped {
   id: string
   title: string
   img: string
   genre: string
   shortDescription: string
   description: string
   gallery: string[]
   reviews: Review[]
   patterns?: Pattern[]
   spaceClient?: {
      data: string
      framework: string
      loader: string
      wasm: string
   }
}

export type ComponentProps<T = {}, E extends ElementType = 'div'> = Omit<
   React.ComponentPropsWithoutRef<E>,
   keyof T
> &
   T

export interface INetwork {
   name: string
   nodeUrl: string
   ipfsUrl: string
}

export interface SpaceWithMetadataUnwrapped extends Omit<Space, 'price'>, SpaceMetadataUnwrapped {
   price: number
}
