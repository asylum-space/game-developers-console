import { find, map } from 'lodash/fp'

import {
   Blueprint,
   Interpretation,
   Item,
   Space,
   TagMetadata,
   AsylumApi as api,
} from '@asylum-ui/connection-library'

import { InjectedAccountWithMeta } from '@polkadot/extension-inject/types'
import {
   BlueprintUnwrapped,
   InterpretationUnwrapped,
   ItemUnwrapped,
   SpaceWithMetadataUnwrapped,
} from 'types'
import { getAllFiles, getFile } from 'utils'

export const uploadMetadata = api.uploadMetadata.bind(api)
export const mintItem = api.mintItem.bind(api)
export const acceptItemUpdate = api.acceptItemUpdate.bind(api)
const spaceMetadataOf = api.spaceMetadataOf.bind(api)

const extendSpace = async (space: Space): Promise<SpaceWithMetadataUnwrapped> => {
   const metadata = await spaceMetadataOf(+space.id)
   const metadataUnwrapped = await getFile(metadata.data)
   return {
      ...metadataUnwrapped,
      title: metadata.title,
      genre: metadata.genre,
      ...space,
   }
}

export const fetchSpaces = async (
   account?: InjectedAccountWithMeta
): Promise<SpaceWithMetadataUnwrapped[]> => {
   let spaces = await api.spaces()

   if (account !== undefined) {
      spaces = spaces.filter((space) => space.owner === account.address)
   }

   return Promise.all(spaces.map(extendSpace))
}

export const fetchSpace = async (id: number): Promise<SpaceWithMetadataUnwrapped> => {
   const space = await api.space(id)
   return extendSpace(space)
}

export const fetchParticipantsCount = async (): Promise<number> => {
   const spacePasses = await api.spacePasses()
   return spacePasses?.length || 0
}

export const fetchTags = async (): Promise<TagMetadata[]> => {
   const tags = await api.tags()
   return (await getAllFiles(map('metadata', tags))) as TagMetadata[]
}

export const fetchBlueprint = async (id: string): Promise<BlueprintUnwrapped> => {
   const blueprint = await api.blueprint(id)
   return extendBlueprint(blueprint)
}

export const fetchBlueprints = async (): Promise<BlueprintUnwrapped[]> => {
   const blueprints = await api.blueprints()
   return Promise.all(blueprints.map((blueprint) => extendBlueprint(blueprint)))
}

export const fetchItem = async (blueprintId: string, id: string): Promise<ItemUnwrapped> => {
   const item = await api.item(blueprintId, id)
   return unwrapItem(item)
}

export const fetchItems = async (ownerAccountId: string): Promise<ItemUnwrapped[]> => {
   const items = await api.itemsByOwner(ownerAccountId)
   return Promise.all(
      items.map((item) => {
         return unwrapItem(item)
      })
   )
}

const extendBlueprint = async (blueprint: Blueprint): Promise<BlueprintUnwrapped> => {
   const interpretations = await fetchBlueprintInterpretationsWithMetadata(blueprint.id)
   const metadata: any = await getFile(blueprint.metadata)
   const defaultInterpretation = find(
      (interpretation) => interpretation.tags.includes('default-view'),
      interpretations
   )

   if (!defaultInterpretation) throw new Error('No default interpretation')

   return {
      ...blueprint,
      interpretations,
      metadata,
      img: defaultInterpretation.interpretationInfo.src || '',
      defaultInterpretation,
   }
}

const fetchBlueprintInterpretationsWithMetadata = async (
   id: string
): Promise<InterpretationUnwrapped[]> => {
   const interpretations = await api.blueprintInterpretations(id)
   return unwrapInterpretations(interpretations)
}

export const fetchItemInterpretations = async (
   blueprintId: string,
   id: string
): Promise<InterpretationUnwrapped[]> => {
   const interpretations = await api.itemInterpretations(blueprintId, id)
   return unwrapInterpretations(interpretations)
}

const unwrapInterpretations = (
   interpretations: Interpretation[]
): Promise<InterpretationUnwrapped[]> => {
   return Promise.all(
      map(
         async (interpretation) =>
            ({
               ...interpretation,
               interpretationInfo: {
                  ...interpretation.interpretationInfo,
                  metadata: await getFile(interpretation.interpretationInfo.metadata || ''),
               },
            } as InterpretationUnwrapped),
         interpretations
      )
   )
}

const unwrapItem = async (item: Item): Promise<ItemUnwrapped> => {
   const interpretations = await fetchItemInterpretations(item.blueprintId, item.id)
   const defaultInterpretation =
      interpretations.find((interpretation) => interpretation.tags.includes('default-view')) ||
      interpretations.find((interpretation) => interpretation.pendingTags?.includes('default-view'))

   const hasPendingInterpretations = !!interpretations.find(
      (interpretation) => interpretation.pending,
      interpretations
   )

   if (!defaultInterpretation) throw new Error('No default interpretation')

   const metadata = await getFile(item.metadata as string)

   return {
      ...item,
      interpretations,
      img: defaultInterpretation?.interpretationInfo.src || '',
      defaultInterpretation,
      hasPendingInterpretations,
      metadata,
   }
}
