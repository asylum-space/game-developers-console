import { ReactChild, ReactNode, useContext, useState } from 'react'

import classNames from 'classnames'
import { Carousel } from 'react-responsive-carousel'
import { twMerge } from 'tailwind-merge'

import { ReactComponent as ArrowRight } from 'assets/svg/arrow-right.svg'
import { ReactComponent as CloseIcon } from 'assets/svg/close.svg'
import { FullscreenPrompt } from 'components/fullscreen-prompt'
import { FullScreenOverlay } from 'components/modal'
import { GalleryStateContext } from 'modules/overview'
import { ComponentProps } from 'types'

interface GalleryProps {
   children: ReactChild[]
   renderThumbs?: (node: ReactChild[]) => ReactChild[]
   selectedIndex?: number
}

const renderArrowNext = (
   action: 'next' | 'prev',
   clickHandler: () => void,
   hasNext: boolean,
   // eslint-disable-next-line @typescript-eslint/no-unused-vars
   label: string
) => {
   return (
      <button
         onClick={clickHandler}
         className={classNames(
            'group absolute inset-y-0 w-12 flex items-center justify-center z-10',
            action === 'next' ? 'right-0' : 'left-0',
            !hasNext && 'pointer-events-none'
         )}
      >
         <ArrowRight
            className={classNames(
               'absolute fill-neutral-500 transition-colors',
               action === 'prev' && 'scale-x-mirror',
               hasNext ? 'group-hover:fill-white' : 'fill-white/0'
            )}
         />
      </button>
   )
}

/**
 * Gallery for images and 3d models.
 * @constructor
 */
export const Gallery = ({ children, renderThumbs, className }: ComponentProps<GalleryProps>) => {
   const [isFullScreen, setIsFullScreen] =
      useContext(GalleryStateContext) || useState<boolean>(false)
   const [isClosing, setIsClosing] = useState(false)

   const handleClose = () => {
      setIsClosing(true)
   }

   const handleOpen = () => {
      setIsFullScreen(true)
   }

   const handleAnimationEnd = () => {
      if (isClosing) {
         setIsClosing(false)
         setIsFullScreen(false)
      }
   }

   const renderItem = (node: ReactNode): ReactNode => {
      return (
         <div className="group h-full w-full relative">
            {node}
            {!isFullScreen && <FullscreenPrompt className="rounded" onClick={handleOpen} />}
         </div>
      )
   }

   return (
      <>
         <div
            className={twMerge(
               classNames({
                  'z-40 fixed top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 bg-neutral-900 p-6 pt-9 pb-3 w-5/6 max-w-5xl rounded':
                     isFullScreen && !isClosing,
               }),
               className
            )}
         >
            <Carousel
               showIndicators={false}
               showArrows={true}
               showThumbs={children.length > 1}
               showStatus={false}
               renderThumbs={renderThumbs}
               renderItem={renderItem}
               renderArrowPrev={(...args) => renderArrowNext('prev', ...args)}
               renderArrowNext={(...args) => renderArrowNext('next', ...args)}
            >
               {children}
            </Carousel>
            {isFullScreen && !isClosing && (
               <CloseIcon
                  onClick={handleClose}
                  className="cursor-pointer absolute right-0 top-0 p-4 w-12 h-12 fill-gray-700 hover:fill-asylum-magenta transition-colors"
               />
            )}
         </div>
         {isFullScreen && !isClosing && (
            // Prevent content jumps during fullscreen open/close events
            <div
               style={{
                  aspectRatio: '2/1.13',
               }}
               className="w-full"
            />
         )}
         {isFullScreen && (
            <FullScreenOverlay
               handleAnimationEnd={handleAnimationEnd}
               handleClose={handleClose}
               open={isFullScreen}
               isClosing={isClosing}
               inPortal={false}
            />
         )}
      </>
   )
}
