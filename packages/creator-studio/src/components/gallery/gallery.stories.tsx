import { ComponentMeta, ComponentStory } from '@storybook/react'

import logo from 'assets/svg/empty.svg'

import { Gallery } from './gallery'

const Image = <img src={logo} alt="logo" />

export default {
   title: 'Gallery',
   component: Gallery,
} as ComponentMeta<typeof Gallery>

const Template: ComponentStory<typeof Gallery> = (args) => <Gallery {...args} />

export const Primary = Template.bind({})

Primary.args = {
   children: [Image, Image, Image],
}
