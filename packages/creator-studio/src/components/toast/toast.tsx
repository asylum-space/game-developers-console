import React from 'react'

import classNames from 'classnames'
import { Slide, ToastContainer } from 'react-toastify'

import { ReactComponent as CloseIcon } from 'assets/svg/close.svg'
import { ComponentProps } from 'types'

interface CloseButtonProps {
   closeToast?: React.MouseEventHandler<SVGSVGElement>
}

const CloseButton = ({ closeToast }: CloseButtonProps) => (
   <CloseIcon
      onClick={closeToast}
      className="mr-1 hover:fill-asylum-magenta transition-all cursor-pointer fill-gray-400"
   />
)

export const Toast = ({ className }: ComponentProps) => {
   return (
      <>
         <ToastContainer
            toastClassName={() =>
               classNames(
                  'relative bg-neutral-700 rounded flex items-center justify-between overflow-hidden py-2 px-4 text-md drop-shadow-xl',
                  className
               )
            }
            closeButton={CloseButton}
            transition={Slide}
            bodyClassName={() => 'overflow-hidden w-full flex items-center gap-2 py-1'}
            position="top-center"
            autoClose={2000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            pauseOnFocusLoss
            draggable={false}
            pauseOnHover
         />
      </>
   )
}
