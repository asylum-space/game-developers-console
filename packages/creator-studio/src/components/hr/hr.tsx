import classNames from 'classnames'
import { twMerge } from 'tailwind-merge'

import { ComponentProps } from 'types'

/**
 * Styled horizontal line
 * @constructor
 */
export const Hr = ({ className }: ComponentProps<{}, 'hr'>) => {
   return (
      // TODO: Create proper styles for active/inactive HR
      <hr className={twMerge(classNames('border-0 bg-gradient-hr my-4 h-0.5 w-full'), className)} />
   )
}
