import { ComponentMeta } from '@storybook/react'

import { Hr } from './hr'

export default {
   title: 'Hr',
   component: Hr,
} as ComponentMeta<typeof Hr>

export const Primary = () => <Hr />
