import { ComponentMeta } from '@storybook/react'

import { JsonRaw } from './json-raw'

export default {
   title: 'JsonRaw',
   component: JsonRaw,
} as ComponentMeta<typeof JsonRaw>

const metadata = {
   name: 'some value',
   surname: 'some value',
   'loves dogs': true,
   'loves cats': true,
   'beloved colors': ['green', 'black', 'blue'],
}

export const Primary = () => <JsonRaw metadata={metadata} />
