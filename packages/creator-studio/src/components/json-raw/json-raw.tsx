import { forwardRef, useState } from 'react'

import classNames from 'classnames'
import hljs from 'highlight.js'
import { toast } from 'react-toastify'

import { ReactComponent as CheckmarkIcon } from 'assets/svg/checkmark.svg'
import { ReactComponent as CopyIcon } from 'assets/svg/copy.svg'
import { ComponentProps } from 'types'

interface JsonRawProps {
   /**
    * data to be styled in Javascript style. Styles are variation of VSCode syntax.
    */
   metadata: any
}

/**
 * Highlighted JS syntax for object in <pre/> element
 */
export const JsonRaw = forwardRef<HTMLPreElement, ComponentProps<JsonRawProps>>(
   ({ metadata, className }, ref) => {
      const [copied, setCopied] = useState(false)
      const CopiedIcon = copied ? CheckmarkIcon : CopyIcon

      const metadataString = JSON.stringify(metadata, null, 2)
      const metadataHtml = hljs.highlight(metadataString, { language: 'json' }).value

      const handleCopy = async () => {
         try {
            await navigator.clipboard.writeText(metadataString)
            setCopied(true)
            setTimeout(() => setCopied(false), 1000)
         } catch (error) {
            if (error instanceof Error) {
               toast.error(error.name)
            }
         }
      }

      return (
         <div className="group relative">
            <div
               onClick={handleCopy}
               className="absolute right-0 top-0 cursor-pointer p-2 copy-wrapper"
            >
               <CopiedIcon className="fill-transparent group-hover:fill-neutral-500 hover:fill-white transition-colors text-base font-secondary" />
            </div>
            <pre
               ref={ref}
               className={classNames(
                  'hljs-json text-sm border-neutral-500 overflow-auto',
                  className
               )}
               dangerouslySetInnerHTML={{ __html: metadataHtml }}
            ></pre>
         </div>
      )
   }
)

JsonRaw.displayName = 'JsonRaw'
