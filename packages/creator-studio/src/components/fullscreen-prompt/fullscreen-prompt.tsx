import classNames from 'classnames'
import { twMerge } from 'tailwind-merge'

import { ReactComponent as SearchIcon } from 'assets/svg/search.svg'
import { ComponentProps } from 'types'

type FullscreenPromptProps = Required<Pick<ComponentProps, 'onClick'>>

/**
 * Simple click catcher with magnifying icon.
 * @constructor
 */
export const FullscreenPrompt = ({ onClick, className }: ComponentProps<FullscreenPromptProps>) => {
   return (
      <div
         onClick={onClick}
         className={twMerge(classNames('group absolute inset-0 cursor-pointer'), className)}
      >
         <div
            className="absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2
                        bg-neutral-900/0 group-hover:bg-neutral-900/50 rounded-full p-2
                        transition-all"
         >
            <SearchIcon className="h-9 w-9 opacity-0 group-hover:opacity-100 transition-all fill-white" />
         </div>
      </div>
   )
}
