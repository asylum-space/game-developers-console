import { ComponentMeta, ComponentStory } from '@storybook/react'

import { FullscreenPrompt } from './fullscreen-prompt'

export default {
   title: 'FullscreenPrompt',
   component: FullscreenPrompt,
   decorators: [
      (Story) => (
         <div className="relative w-96 h-96 m-auto bg-cyan-300 text-black flex justify-center items-center">
            Hover me!
            <Story />
         </div>
      ),
   ],
} as ComponentMeta<typeof FullscreenPrompt>

const Template: ComponentStory<typeof FullscreenPrompt> = (args) => <FullscreenPrompt {...args} />

export const Primary = Template.bind({})
