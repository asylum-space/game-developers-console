import classNames from 'classnames'
import { twMerge } from 'tailwind-merge'

import { Tag } from 'components/tag'
import { ComponentProps } from 'types'

interface TagGroupProps {
   tags: string[]
}

/**
 * Representative group of flags combined into styled row
 * @constructor
 */
export const TagGroup = ({ tags, className }: ComponentProps<TagGroupProps>) => {
   return (
      <div
         className={twMerge(classNames('flex overflow-hidden'), className)}
         data-testid="tag-group"
      >
         {tags.sort().map((tag, index) => (
            <Tag
               key={index}
               data-testid="tag"
               className="rounded-none relative first:rounded-tl-xl last:rounded-br-xl after:last:hidden after:absolute after:top-[-0.250rem] after:right-0 after:w-0.5 after:h-[125%] after:bg-black/30 after:rotate-[15deg] after:z-10 my-0.5"
            >
               {tag}
            </Tag>
         ))}
      </div>
   )
}
