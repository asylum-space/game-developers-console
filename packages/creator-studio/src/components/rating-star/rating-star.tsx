import React from 'react'

import { ReactComponent as EmptyStarIcon } from 'assets/svg/empty-star.svg'
import { ReactComponent as FillStarIcon } from 'assets/svg/fill-star.svg'
import { ReactComponent as HalfStarIcon } from 'assets/svg/half-star.svg'
import { ComponentProps } from 'types'

interface RatingStarsComponentProps {
   /**
    * number 1 to 5.
    */
   rating: number
}

/**
 * 5 stars row. Stars are highlighted according to rating.
 * @constructor
 */
export const RatingStarsComponent = ({ rating = 0 }: ComponentProps<RatingStarsComponentProps>) => {
   const starFilter = (): React.ReactNode => {
      const starArray: React.ReactNode[] = []
      for (let i = 0; i < 5; i++) {
         if (rating <= i) {
            starArray.push(<EmptyStarIcon className="fill-neutral-300" key={i} />)
         } else if (rating > i && rating < i + 1) {
            starArray.push(<HalfStarIcon className="fill-neutral-300" key={i} />)
         } else {
            starArray.push(<FillStarIcon className="fill-neutral-300" key={i} />)
         }
      }
      return starArray
   }

   return <div className="flex gap-3">{starFilter()}</div>
}
