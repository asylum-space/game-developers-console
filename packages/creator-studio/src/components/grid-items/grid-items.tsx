import classNames from 'classnames'
import { twMerge } from 'tailwind-merge'

import { ComponentProps } from 'types'

interface GridItemsProps extends ComponentProps {
   dataTestId?: string
}

/**
 * Creates basic grid from its children
 * @constructor
 */
export const GridItems = ({ className, dataTestId = 'grid-items', children }: GridItemsProps) => {
   return (
      <div
         className={twMerge(classNames('grid lg:grid-cols-2 xl:grid-cols-3 gap-10'), className)}
         data-testid={dataTestId}
      >
         {children}
      </div>
   )
}
