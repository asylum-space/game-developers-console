import classNames from 'classnames'
import { twMerge } from 'tailwind-merge'

import { ComponentProps } from 'types'

export const Heading = ({ className, children }: ComponentProps<{}, 'h3'>) => {
   return (
      <h3 className={twMerge(classNames('font-normal text-lg text-inherit'), className)}>
         {children}
      </h3>
   )
}
