import classNames from 'classnames'
import { twMerge } from 'tailwind-merge'

import { ComponentProps } from 'types'

export const Paragraph = ({ className, children }: ComponentProps<{}, 'p'>) => {
   return <p className={twMerge(classNames('font-normal text-base'), className)}>{children}</p>
}
