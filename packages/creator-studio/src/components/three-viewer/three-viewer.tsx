import '@google/model-viewer'
import { ModelViewerElement } from '@google/model-viewer/lib/model-viewer'
import classNames from 'classnames'
import { twMerge } from 'tailwind-merge'

import { ComponentProps } from 'types'

declare global {
   // eslint-disable-next-line no-unused-vars
   namespace JSX {
      // eslint-disable-next-line no-unused-vars
      interface IntrinsicElements {
         'model-viewer': Partial<ModelViewerElement & { class: string }>
      }
   }
}

interface ThreeViewerProps {
   className?: string
}

/**
 * 3d-model render engine for app. Implements modal-viewer from @google
 * @see https://modelviewer.dev/
 * @constructor
 */
export const ThreeViewer = ({
   className,
   ...rest
}: ComponentProps<ThreeViewerProps, 'model-viewer'>) => {
   return (
      <model-viewer
         class={twMerge(classNames('w-auto --poster-color: transparent'), className)}
         {...rest}
      />
   )
}
