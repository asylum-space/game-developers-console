import classNames from 'classnames'
import { twMerge } from 'tailwind-merge'

import { InputLabel } from 'components/input'
import { ComponentProps } from 'types'

interface InputFieldSkeletonProps {
   label?: string
   labelTooltipMessage?: string
   inputClassName?: string
}

/**
 * Skeleton for input field
 * @constructor
 */
export const InputFieldSkeleton = ({
   label,
   labelTooltipMessage,
   className,
   inputClassName,
}: ComponentProps<InputFieldSkeletonProps>) => {
   return (
      <>
         <div
            className={twMerge(classNames('w-full relative flex flex-col items-start'), className)}
         >
            {label && <InputLabel tooltipMessage={labelTooltipMessage}>{label}</InputLabel>}
            <input
               className={twMerge(
                  classNames('rounded w-full py-3 bg-gray-500 animate-pulse'),
                  inputClassName
               )}
               disabled
            />
         </div>
      </>
   )
}
