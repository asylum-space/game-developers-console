/**
 * Skeleton for Card-link
 * @constructor
 * @see ListCardItem
 */
export const ListCardItemSkeleton = () => {
   return <div className="bg-white p-5 rounded animate-pulse opacity-75 h-[407px]"></div>
}
