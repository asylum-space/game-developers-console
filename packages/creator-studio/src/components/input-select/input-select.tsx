import Select, {
   ClearIndicatorProps,
   DropdownIndicatorProps,
   GroupBase,
   Props,
   components,
} from 'react-select'

import { ReactComponent as ArrowDownIcon } from 'assets/svg/arrow-down.svg'
import { ReactComponent as CloseIcon } from 'assets/svg/close.svg'
import { Paragraph } from 'components/text'
import { colorVariants } from 'context'

const ClearIndicator = <
   Option,
   IsMulti extends boolean = false,
   Group extends GroupBase<Option> = GroupBase<Option>
>(
   props: ClearIndicatorProps<Option, IsMulti, Group>
) => {
   return (
      <components.ClearIndicator {...props}>
         <CloseIcon />
      </components.ClearIndicator>
   )
}

const DropdownIndicator = <Option, IsMulti extends boolean = false>(
   props: DropdownIndicatorProps<Option, IsMulti>
) => {
   return (
      <components.DropdownIndicator {...props}>
         <ArrowDownIcon />
      </components.DropdownIndicator>
   )
}

export interface InputSelectProps<
   Option,
   IsMulti extends boolean = false,
   Group extends GroupBase<Option> = GroupBase<Option>
> extends Props<Option, IsMulti, Group> {
   errorMessage?: string
   surroundColor?: colorVariants
   dataTestId?: string
}

// TODO: Utilize theme concept for it's max potential
/**
 * React select with styles implemented,
 * @constructor
 */
export const InputSelect = <
   Option,
   IsMulti extends boolean = false,
   Group extends GroupBase<Option> = GroupBase<Option>
>({
   errorMessage,
   styles,
   components,
   surroundColor = 'black',
   dataTestId = 'input-select',
   ...rest
}: InputSelectProps<Option, IsMulti, Group>) => {
   return (
      <div onClick={(event) => event.stopPropagation()} data-testid={dataTestId}>
         <Select
            theme={(theme) => ({
               ...theme,
               colors: {
                  ...theme.colors,
                  neutral0: surroundColor === 'black' ? '#1C1C1C' : '#404040',
                  primary: surroundColor === 'black' ? '#292929' : '#525252',
                  primary25: surroundColor === 'black' ? '#292929' : '#525252',
                  primary50: surroundColor === 'black' ? '#292929' : '#525252',
                  neutral5: '#292929',
               },
            })}
            styles={{
               control: (base) => ({
                  ...base,
                  cursor: 'text',
                  fontSize: '0.875rem',
                  lineHeight: '1.25rem',
                  borderRadius: '0.3125rem',
                  border: 'none',
                  outline: '2px solid transparent',
                  outlineColor: errorMessage ? '#F87171' : 'transparent',
                  paddingTop: '0.25rem',
                  paddingBottom: '0.25rem',
               }),
               menu: (base) => ({
                  ...base,
                  boxShadow: '0 20px 25px -5px rgb(0 0 0 / 0.1), 0 8px 10px -6px rgb(0 0 0 / 0.1)',
                  overflow: 'hidden',
                  zIndex: 20,
                  borderRadius: '0.3125rem',
                  fontSize: '0.875rem',
                  lineHeight: '1.25rem',
                  color: 'white',
               }),
               singleValue: (base) => ({
                  ...base,
                  color: 'white',
               }),
               multiValue: (base, state) => ({
                  ...base,
                  backgroundColor: state.isDisabled ? '#2d6789' : '#4396C6',
                  borderTopLeftRadius: '1rem',
                  borderBottomRightRadius: '1rem',
               }),
               multiValueLabel: (base) => ({
                  ...base,
                  fontSize: '0,875rem',
                  color: 'white',
                  paddingLeft: '0.5rem',
               }),
               multiValueRemove: (base) => ({
                  ...base,
                  color: 'white',
                  ':hover': {
                     ...base[':hover'],
                     borderBottomRightRadius: '1rem',
                  },
               }),
               indicatorsContainer: (base) => ({
                  ...base,
                  marginRight: '0.375rem',
               }),
               dropdownIndicator: (base) => ({
                  ...base,
                  cursor: 'pointer',
                  fill: '#737373',
                  transition: 'fill 0.15s',
                  '&:hover': {
                     fill: 'white',
                  },
               }),
               clearIndicator: (base) => ({
                  ...base,
                  cursor: 'pointer',
                  fill: '#737373',
                  transition: 'fill 0.15s',
                  '&:hover': {
                     fill: 'white',
                  },
               }),
               option: (base) => ({
                  ...base,
                  paddingLeft: 'rem',
                  paddingRight: '1rem',
                  cursor: 'pointer',
               }),
               indicatorSeparator: (base) => ({
                  ...base,
                  display: 'none',
               }),
               input: (base) => ({
                  ...base,
                  color: '#DFDFDF',
               }),
               placeholder: (base) => ({
                  ...base,
                  color: '#a3a3a3',
               }),
               ...styles,
            }}
            components={{
               ClearIndicator,
               DropdownIndicator,
               ...components,
            }}
            {...rest}
         />
         {errorMessage && <Paragraph className="text-red-400 ml-2 mt-2">{errorMessage}</Paragraph>}
      </div>
   )
}
