import { ReactNode, forwardRef } from 'react'

import classNames from 'classnames'
import { twMerge } from 'tailwind-merge'

import { ComponentProps } from 'types'

interface CardProps {
   /**
    * @type react element to be displayed in header of the card. Card inject styles to it.
    */
   header?: ReactNode
   /**
    * classes to be injected in header
    */
   headerClassName?: string
   /**
    * classes to be injected in content
    */
   contentClassName?: string
   dataTestId?: string
}

/**
 * Generic Card. Should be used as wrapper with a title. Title should be provided as prop
 */
export const Card = forwardRef<HTMLDivElement, ComponentProps<CardProps>>(
   (
      {
         className,
         header,
         headerClassName,
         contentClassName,
         dataTestId = 'card',
         children,
         ...rest
      },
      ref
   ) => {
      return (
         <div
            ref={ref}
            className={twMerge(
               classNames('p-px w-full flex items-stretch flex-col rounded'),
               className
            )}
            data-testid={dataTestId}
            {...rest}
         >
            {header && (
               <header
                  className={twMerge(
                     classNames('bg-neutral-800 px-8 py-5 rounded-t'),
                     headerClassName
                  )}
               >
                  {header}
               </header>
            )}
            <div
               className={twMerge(
                  classNames('grow bg-neutral-900 px-8 py-5 rounded-b', !header && 'rounded-t'),
                  contentClassName
               )}
            >
               {children}
            </div>
         </div>
      )
   }
)

Card.displayName = 'Card'
