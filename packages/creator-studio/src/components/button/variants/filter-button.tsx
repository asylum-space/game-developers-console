import { ReactComponent as FilterIcon } from 'assets/svg/filter.svg'
import { Button } from 'components/button'
import { colorVariants } from 'context'

interface FilterButtonProps {
   filteredData: any[]
   onClick: () => void
   surroundColor?: colorVariants
}

export const FilterButton = ({ filteredData, onClick, surroundColor }: FilterButtonProps) => {
   return (
      <Button
         variant="dark"
         className="shrink-0 bg-neutral-800"
         onClick={onClick}
         surroundColor={surroundColor}
      >
         {filteredData.length === 0 ? (
            <div className="flex items-center gap-2 group">
               <FilterIcon className="w-5 h-5" />
               Filter
            </div>
         ) : (
            `${filteredData.length} selected`
         )}
      </Button>
   )
}
