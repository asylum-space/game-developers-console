import { ReactNode } from 'react'

import classNames from 'classnames'
import { twMerge } from 'tailwind-merge'

import { Button, ButtonProps } from 'components/button'
import { colorVariants } from 'context'
import { ComponentProps } from 'types'

interface ButtonWithIconProps extends ButtonProps {
   icon: ReactNode
   surroundColor?: colorVariants
}

export const ButtonWithIcon = ({
   icon,
   children,
   className,
   surroundColor,
   ...rest
}: ComponentProps<ButtonWithIconProps, 'button'>) => {
   return (
      <div className={classNames('flex justify-between items-center gap-4')}>
         <div className="w-5 h-5 flex items-center justify-center">{icon}</div>
         <Button
            variant="dark"
            className={twMerge(classNames('text-left grow'), className)}
            surroundColor={surroundColor}
            {...rest}
         >
            {children}
         </Button>
      </div>
   )
}
