import classNames from 'classnames'
import { twMerge } from 'tailwind-merge'

import { colorVariants } from 'context'
import { ComponentProps } from 'types'

export interface ButtonProps {
   /**
    * boolean value, shows error styles.
    */
   error?: boolean
   /**
    * main theme of the button.
    */
   variant?: 'light' | 'dark' | 'success'
   /**
    * alter colors a bit in 'dark' variant depending on surrounding element, either dark or gray.
    */
   surroundColor?: colorVariants
}

// TODO: discuss light+disabled variant
/**
 * Generic button component for the app.
 */
export const Button = ({
   variant = 'light',
   surroundColor = 'black',
   className,
   error,
   disabled,
   children,
   ...rest
}: ComponentProps<ButtonProps, 'button'>) => {
   return (
      <button
         className={twMerge(
            classNames(
               'group rounded px-5 py-2 pt-2.5 text-base text-center font-secondary tracking-wide select-none',
               variant === 'light' && disabled && 'text-neutral-400',
               variant === 'light' && disabled && surroundColor === 'black' && 'bg-neutral-900',
               variant === 'light' && disabled && surroundColor === 'gray' && 'bg-neutral-800',
               variant === 'light' && !disabled && 'bg-neutral-300 text-neutral-800',
               variant === 'light' && !disabled && !error && 'border-glitch-effect',

               variant === 'dark' && 'bg-neutral-900',
               variant === 'dark' && disabled && 'text-neutral-500',
               variant === 'dark' && !disabled && 'text-white',
               variant === 'dark' &&
                  !disabled &&
                  !error &&
                  'hover:bg-neutral-600 transition-colors',

               error && 'border-1 border-red-500'
            ),
            className
         )}
         disabled={disabled}
         {...rest}
      >
         {children}
      </button>
   )
}
