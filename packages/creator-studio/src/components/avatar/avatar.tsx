import classNames from 'classnames'
import { twMerge } from 'tailwind-merge'

import empty from 'assets/svg/empty.svg'
import { ComponentProps } from 'types'
import { ipfsPath } from 'utils'

interface AvatarProps {
   /**
    * deterministic size of the avatar component
    */
   size?: 'lg' | 'sm'
   /**
    * URI of the source image
    */
   src?: string
}

/**
 * Shows user avatar from blockchain
 * @alpha
 * @constructor
 */
export const Avatar = ({ src, size = 'sm', className }: ComponentProps<AvatarProps>) => {
   return (
      <div
         style={{
            backgroundImage: src ? `url('${ipfsPath(src)}')` : `url(${empty})`,
         }}
         className={twMerge(
            classNames(
               'bg-cover bg-center',
               {
                  'w-44 h-44 rounded': size === 'lg',
                  'w-8 h-8 rounded-full': size === 'sm',
               },
               className
            )
         )}
      />
   )
}
