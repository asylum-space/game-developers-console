import { ReactNode } from 'react'

import { NavLink } from 'react-router-dom'

import { ButtonWithIcon } from 'components/button'
import { ComponentProps } from 'types'

interface SidebarLinkProps {
   to: string
   icon: ReactNode
}

export const SidebarLink = ({
   to,
   children,
   ...rest
}: ComponentProps<SidebarLinkProps, 'button'>) => {
   return (
      <NavLink to={to} className="select-none">
         <ButtonWithIcon {...rest}>{children}</ButtonWithIcon>
      </NavLink>
   )
}
