import classNames from 'classnames'
import { observer } from 'mobx-react-lite'

import { ReactComponent as CircleIcon } from 'assets/svg/circle.svg'
import { ReactComponent as CloseIcon } from 'assets/svg/close.svg'
import { ReactComponent as PlayIcon } from 'assets/svg/play.svg'
import { ReactComponent as RhombIcon } from 'assets/svg/rhomb.svg'
import { ReactComponent as SquareIcon } from 'assets/svg/square.svg'
import { ReactComponent as TriangleIcon } from 'assets/svg/triangle.svg'
import { Avatar } from 'components/avatar'
import { Button } from 'components/button'
import { HeadingLg, Paragraph } from 'components/text'
import { SidebarLink } from 'layout/sidebar/sidebar-link'
import { NodeConnect } from 'modules/node-connect'
import { useStore } from 'store'

export const Sidebar = observer(() => {
   const store = useStore()

   const iconClassName = 'stroke-neutral-300'

   return (
      <aside className="hidden md:flex w-52 lg:w-72 fixed inset-y-0 mt-32 bg-neutral-900 flex-col items-center rounded-t">
         <div
            className={classNames(
               'w-full flex flex-col items-center gap-5 bg-neutral-800 rounded-t',
               store.selectedSpace ? 'pb-6' : 'pb-12'
            )}
         >
            <Avatar
               className="w-48 -mt-24 h-auto aspect-square"
               size="lg"
               src={store.selectedSpace?.img}
            />
            {store.selectedSpace && (
               <>
                  <div className="flex flex-col items-center gap-1">
                     <HeadingLg>{store.selectedSpace?.title || ''}</HeadingLg>
                     <Paragraph>{store.selectedSpace?.genre || ''}</Paragraph>
                  </div>
                  <div className="flex gap-4 items-stretch">
                     <Button
                        className="flex gap-2 items-center px-4"
                        disabled={
                           !store.selectedSpace ||
                           !store.account ||
                           !store.selectedSpace?.spaceClient
                        }
                        onClick={() => {
                           store.setIsInterfaceOpen(false)
                           store.setIsSpaceRunning(true)
                        }}
                     >
                        <PlayIcon /> {!store.isSpaceRunning ? 'RUN' : 'open space'}
                     </Button>

                     {store.isSpaceRunning && (
                        <Button
                           className="flex gap-2 items-center px-4"
                           disabled={!store.selectedSpace || !store.account}
                           onClick={async () => {
                              await store.unityContext.quitUnityInstance()
                              store.setIsSpaceRunning(false)
                           }}
                        >
                           <CloseIcon className={iconClassName} /> close
                        </Button>
                     )}
                  </div>
               </>
            )}
         </div>
         <div className="w-full flex flex-col justify-between p-8 pt-6 h-full">
            <nav className="flex flex-col gap-5">
               <SidebarLink icon={<TriangleIcon className={iconClassName} />} to="/">
                  select space
               </SidebarLink>
               <SidebarLink
                  icon={<SquareIcon className={iconClassName} />}
                  to="/overview"
                  disabled={!store.account || !store.isConnected || !store.selectedSpace}
               >
                  space overview
               </SidebarLink>
               <SidebarLink
                  icon={<RhombIcon className={iconClassName} />}
                  to="/blueprints"
                  disabled={!store.account || !store.isConnected}
               >
                  blueprints
               </SidebarLink>
               <SidebarLink
                  icon={<CircleIcon className={iconClassName} />}
                  to="/items"
                  disabled={!store.account || !store.isConnected}
               >
                  items
               </SidebarLink>
            </nav>
            <NodeConnect />
         </div>
      </aside>
   )
})
