import { ComponentProps } from 'types'

export const Header = ({ children }: ComponentProps) => {
   return <header className="mb-10 container mx-auto flex justify-end">{children}</header>
}
