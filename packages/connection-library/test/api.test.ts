import { IAsylumApi, getFile } from '../src'
import { connect, dockerRestart, seed } from './commands'

const SEEDER_ADDRESS = '5ELYXe9CdJupDtjymmk5aK1i1XkisWi1X9V6UHHjVSudHmCn'
const CID_REGEX =
   /^Qm[1-9A-HJ-NP-Za-km-z]{44,}|b[A-Za-z2-7]{58,}|B[A-Z2-7]{58,}|z[1-9A-HJ-NP-Za-km-z]{48,}|F[0-9A-F]{50,}$/

let api: IAsylumApi
jest.spyOn(console, 'error').mockImplementation(() => {})

describe('utilities', () => {
   beforeAll(async () => {
      dockerRestart()
      seed()

      api = await connect()

      return api
   })

   afterAll(async () => {
      await api.disconnect()
   })

   it('loads and downloads metadata from/to ipfs', async () => {
      const data = {
         testProp: 'testValue',
      }
      const CID = await api.uploadMetadata(data)
      const responseData = await getFile(
         process.env.IPFS_NODE_URL?.replace(':5001', ':8080') || '',
         CID
      )

      expect(responseData).toEqual(data)
   })

   it('uploads Files', async () => {
      const encoder = new TextEncoder()
      const encodeString = 'test string'
      const buffer = encoder.encode(encodeString)
      const CID = await api.uploadFile(buffer)

      const returnValue = await getFile(
         process.env.IPFS_NODE_URL?.replace(':5001', ':8080') || '',
         CID
      )
      expect(returnValue).toBe(encodeString)
   })

   it('calculates metadata CID', async () => {
      const data = {
         testProp: 'testValue',
      }
      const CID = await api.getMetadataCID(data)
      expect(CID).toMatch(CID_REGEX)
   })

   it('returns polkadot api', () => {
      expect(api.polkadotApi).toBeDefined()
   })
})

describe('API queries', () => {
   beforeAll(async () => {
      dockerRestart()
      seed()

      api = await connect()

      return api
   })

   afterAll(async () => {
      await api.disconnect()
   })

   const SPACE_PROPERTIES = [
      'admins',
      'assets',
      'attributes',
      'blueprints',
      'freezers',
      'isFrozen',
      'issuers',
      'owner',
      'price',
   ] as const

   it('queries all spaces', async () => {
      const spaces = await api.spaces()
      expect(spaces).toHaveLength(10)
      spaces.forEach((space) => {
         SPACE_PROPERTIES.forEach((prop) => {
            expect(space).toHaveProperty(prop)
         })
         expect(+space.id).not.toBeNaN()
         expect(+space.price).not.toBeNaN()
         expect(space.owner).toEqual(SEEDER_ADDRESS)
         expect(space.admins).toContain(SEEDER_ADDRESS)
         expect(space.freezers).toContain(SEEDER_ADDRESS)
         expect(space.issuers).toContain(SEEDER_ADDRESS)
      })
   })

   it('queries exact space', async () => {
      const space = await api.space(0)
      SPACE_PROPERTIES.forEach((prop) => {
         expect(space).toHaveProperty(prop)
      })
      expect(+space.price).not.toBeNaN()
      expect(space.owner).toEqual(SEEDER_ADDRESS)
      expect(space.admins).toContain(SEEDER_ADDRESS)
      expect(space.freezers).toContain(SEEDER_ADDRESS)
      expect(space.issuers).toContain(SEEDER_ADDRESS)
   })

   it('queries proper metadata of exact space', async () => {
      const metadata = await api.spaceMetadataOf(0)
      expect(metadata.data).toMatch(CID_REGEX)
      expect(metadata.title).toBe('Ninja Rian')
      expect(metadata.genre).toBe('Platformer')
   })

   it('fetches space passes', async () => {
      const passes = await api.spacePasses()
      expect(Array.isArray(passes)).toBe(true)
   })

   it('queries all tags', async () => {
      const tags = await api.tags()
      expect(Array.isArray(tags)).toBe(true)
      expect(tags).toHaveLength(16)
      tags.forEach((tag) => {
         expect(tag).toHaveProperty('id')
         expect(tag).toHaveProperty('approved')
         expect(tag.metadata).toMatch(CID_REGEX)
      })
   })

   const BLUEPRINT_PROPERTIES = ['id', 'name', 'max', 'metadata', 'issuer', 'nftsCount'] as const

   it('queries all blueprints', async () => {
      const blueprints = await api.blueprints()
      expect(blueprints).toHaveLength(4)
      blueprints.forEach((blueprint) => {
         BLUEPRINT_PROPERTIES.forEach((prop) => {
            expect(blueprint).toHaveProperty(prop)
         })

         expect(+blueprint.max).not.toBeNaN()
         expect(+blueprint.max).toBe(100)

         expect(blueprint.metadata).toMatch(CID_REGEX)
      })
   })

   it('queries blueprint by issuer', async () => {
      const blueprints = await api.blueprintsByIssuer(SEEDER_ADDRESS)
      expect(blueprints).toHaveLength(4)
   })

   it('queries exact blueprint', async () => {
      const blueprint = await api.blueprint('1')
      expect(blueprint.id).toBe('1')
      expect(blueprint.issuer).toBe(SEEDER_ADDRESS)
      expect(+blueprint.max).not.toBeNaN()
      expect(blueprint.name).toBe('Clover leaf')
      expect(+blueprint.nftsCount).not.toBeNaN()
      expect(+blueprint.nftsCount).toBe(3)
   })

   const ITEM_PROPERTIES = [
      'id',
      'blueprintId',
      'owner',
      'royalty',
      'pending',
      'metadata',
      'transferable',
   ] as const

   it('queries all items', async () => {
      const items = await api.items()
      expect(items).toHaveLength(12)
      const blueprintIds = [0, 1, 2, 3]
      items.forEach((item) => {
         ITEM_PROPERTIES.forEach((prop) => {
            expect(item).toHaveProperty(prop)
         })
      })
      blueprintIds.forEach((blueprintId) => {
         expect(items.filter((items) => items.blueprintId === blueprintId.toString())).toHaveLength(
            3
         )
      })
   })

   it('queries all items of exact owner', async () => {
      const items = await api.itemsByOwner(SEEDER_ADDRESS)
      expect(items).toHaveLength(12)
      expect(items.every((item) => item.owner.AccountId === SEEDER_ADDRESS)).toBe(true)
   })

   it('queries exact item', async () => {
      const item = await api.item('0', '1')
      ITEM_PROPERTIES.forEach((prop) => {
         expect(item).toHaveProperty(prop)
      })
      expect(item.id).toBe('1')
      expect(item.blueprintId).toBe('0')
      expect(item.metadata).toMatch(CID_REGEX)
      expect(item.owner).toEqual({ AccountId: SEEDER_ADDRESS })
   })

   it('queries items minted from exact blueprint', async () => {
      const items = await api.itemsByBlueprint('0')
      expect(items.length).toBe(3)
   })

   const INTERPRETATION_PROPERTIES = ['tags', 'interpretationInfo', 'id']

   it('queries all interpretations of exact item', async () => {
      const interpretations = await api.itemInterpretations('0', '0')
      expect(interpretations).toHaveLength(4)
      interpretations.forEach((interpretation) => {
         INTERPRETATION_PROPERTIES.forEach((prop) => {
            expect(interpretation).toHaveProperty(prop)
         })
      })
      expect(interpretations).toEqual(
         expect.arrayContaining([
            expect.objectContaining({ id: '0:0:0' }),
            expect.objectContaining({ id: '0:0:1' }),
            expect.objectContaining({ id: '0:0:2' }),
            expect.objectContaining({ id: '0:0:3' }),
         ])
      )
   })

   it('queries interpretations of exact blueprint', async () => {
      const interpretations = await api.blueprintInterpretations('1')
      expect(interpretations).toHaveLength(3)
      expect(interpretations).toEqual(
         expect.arrayContaining([
            expect.objectContaining({ id: '1:0' }),
            expect.objectContaining({ id: '1:1' }),
            expect.objectContaining({ id: '1:2' }),
         ])
      )
      interpretations.forEach((interpretation) => {
         INTERPRETATION_PROPERTIES.forEach((prop) => {
            expect(interpretation).toHaveProperty(prop)
         })
      })
      const intZero = interpretations.find((testInt) => testInt.id === '1:0')

      expect(intZero).not.toBeUndefined()
      expect(intZero?.tags).toEqual(expect.arrayContaining(['default-view', 'png']))
   })
})

describe('API transactions', () => {
   beforeEach(async () => {
      // dockerRestart()
      // seed()

      api = await connect()

      return api
   })

   afterEach(async () => {
      await api.disconnect()
   })

   it('creates blueprint', async () => {
      const blueprintsBefore = (await api.blueprints()).length
      const blueprintName = 'Test name'
      const metadata = {
         description: 'blueprint test description',
      }
      const metadataCID = await api.uploadMetadata(metadata)
      const max = 100
      const tags = (await api.tags()).filter((tag) => tag.id !== 'default-view')
      const interpretations = [
         {
            tags: [...tags.slice(0, 2).map((tag) => tag.id), 'default-view'],
            interpretation: {
               src: 'interpretationSrc1',
               metadata: 'interpretationMetadata1',
            },
         },
         {
            tags: [...tags.slice(3, 5)].map((tag) => tag.id),
            interpretation: {
               src: 'interpretationSrc2',
               metadata: 'interpretationMetadata2',
            },
         },
      ]
      await api.createBlueprint(blueprintName, metadataCID, max, interpretations)
      await new Promise((resolve) => setTimeout(() => resolve(true), 2000))
      const blueprintAfter = (await api.blueprints()).length
      expect(blueprintAfter).toBe(blueprintsBefore + 1)
   })

   it('throws when transaction fails', async () => {
      try {
         await api.createSpace(123, [SEEDER_ADDRESS], -10)
      } catch (e) {
         expect(e).toBeInstanceOf(Error)
      }
   })
})

export {}
