import { AsylumApi, IAsylumApi } from '../src'
import { Keyring } from '@polkadot/api'
import * as cmd from 'child_process'

export function dockerRestart() {
   cmd.execSync('yarn docker:node:restart')
}

export function seed() {
   cmd.execSync('yarn seed:local', { stdio: 'ignore' })
}

export async function connect(): Promise<IAsylumApi> {
   const api = await AsylumApi.connect(
      {
         nodeUrl: process.env.ASYLUM_NODE_URL as string,
         ipfsUrl: process.env.IPFS_NODE_URL as string,
      },
      undefined,
      undefined,
      {
         throwOnConnect: false,
         throwOnUnknown: false,
      }
   )
   await api.api?.isReady
   const seeder = new Keyring({ type: 'sr25519' }).addFromUri(process.env.SEEDER_MNEMONIC || '')
   return api.withKeyringPair(seeder)
}
