import { ApiPromise } from '@polkadot/api'
import { Codec } from '@polkadot/types-codec/types/codec'

export type TagName = string
export type InterpretationId = string
export type CID = string

export interface InterpretationPattern {
   name?: string
   description?: string
   tags?: string[]
   fields?: {
      name: string
      value?: string | number
   }[]
}

export interface Pattern {
   name: string
   description: string
   img: string
   interpretations: InterpretationPattern[]
}

export interface Space {
   id: string
   owner: string
   issuers: string[]
   admins: string[]
   freezers: string[]
   price: string
   instances: number
   instanceMetadatas: number
   attributes: number
   isFrozen: boolean
   blueprints: string[] | null
   assets: number[] | null
   allowUnprivilegedMint: boolean
}

export interface SpaceMetadata {
   data: CID
   title: string
   genre: string
}

export interface Review {
   id: string
   name?: string
   text?: string
   date?: string
   rating: number
   address: string
}

export interface Blueprint {
   id: string
   name: string
   max: string
   metadata: CID
   issuer: string
   nftsCount: number
}

export interface BlueprintCreate {
   name: string
   metadata: CID
}

export interface Item {
   blueprintId: string
   equipped: boolean
   id: string
   metadata: CID
   owner: {
      AccountId: string
   }
   pending: boolean
   royalty: string | null
   transferable: boolean
}

export interface InterpretationInfo {
   src?: string
   metadata?: CID
}

export interface Tag {
   approved: boolean
   metadata: CID
   id: TagName
}

export interface TagMetadataField {
   editable: boolean
   name: string
   type: string
   defaultValue?: string
   description?: string
}

export interface TagMetadata {
   id: TagName
   description: string
   metadataExtensions: { fields: TagMetadataField[] }
}

export interface InterpretationCreate {
   tags: TagName[]
   interpretation: InterpretationInfo
}

export interface Interpretation {
   tags: TagName[]
   interpretationInfo: InterpretationInfo
   id: InterpretationId
   pending?: boolean
   pendingTags?: TagName[]
   pendingRemoval?: boolean
}


export interface ICodecConvertor {
   convert(api: ApiPromise): Codec
}

export class BlueprintChangeAdd implements ICodecConvertor {
   interpretations: InterpretationCreate[]

   constructor(interpretations: InterpretationCreate[]) {
      this.interpretations = interpretations
   }

   convert(api: ApiPromise): Codec {
      return api?.createType('Change', {
         Add: {
            interpretations: this.interpretations,
         },
      })
   }
}

export class BlueprintChangeModify implements ICodecConvertor {
   interpretationUpdates: [InterpretationId, InterpretationCreate][]

   constructor(interpretationUpdates: [InterpretationId, InterpretationCreate][]) {
      this.interpretationUpdates = interpretationUpdates
   }

   convert(api: ApiPromise): Codec {
      return api.createType('Change', {
         Modify: {
            interpretations: this.interpretationUpdates,
         },
      })
   }
}

export class BlueprintChangeRemoveInterpretation implements ICodecConvertor {
   interpretationId: InterpretationId

   constructor(interpretationId: InterpretationId) {
      this.interpretationId = interpretationId
   }

   convert(api: ApiPromise): Codec {
      return api?.createType('Change', {
         RemoveInterpretation: {
            interpretationId: this.interpretationId,
         },
      })
   }
}

export declare type ChangeSet = Array<
   BlueprintChangeAdd | BlueprintChangeModify | BlueprintChangeRemoveInterpretation
>
