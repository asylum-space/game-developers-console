import { apiTypes } from './api-types'
import {
   Blueprint,
   ChangeSet,
   Interpretation,
   InterpretationCreate,
   Item,
   Space,
   SpaceMetadata,
   Tag,
   TagName,
} from './types'
import { handleTxCallback, itemConverter, mapEntries } from './utils'
import { ApiPromise, SubmittableResult, WsProvider } from '@polkadot/api'
import { ApiTypes } from '@polkadot/api-base/types/base'
import { SubmittableExtrinsic } from '@polkadot/api-base/types/submittable'
import { ApiOptions, Signer } from '@polkadot/api/types'
import { KeyringPair } from '@polkadot/keyring/types'
import { create } from 'ipfs-http-client'
import { omit } from 'lodash/fp'

class AsylumApi {
   api: ApiPromise | undefined
   provider: WsProvider | undefined
   keyringPair: KeyringPair | undefined
   address: string | undefined
   injectedSigner: Signer | undefined
   ipfsUrl: string | undefined

   /**
    * Connects web client to the node and sets IPFS endpoint.
    * @param nodeUrl websocket url of the node, ipv4.
    * @param ipfsUrl url of the IPFS, ipv4.
    * @param onConnected callback that will be called after connection, should be used for successful connection
    * @param onDisconnected callback that will be called after disconnection from node, if occurred during session
    * @param options addition options for the blockchain connector
    * @throws Throw any error if connection failed, for example when internet connection was lost.
    * @returns instance of api
    * @example
    * ```ts
    * AsylumApi.connect({
    *    node: ws://127.0.0.1:9944,
    *    ipfs: http://127.0.0.1:5001,
    * })
    * ```
    */
   async connect(
      { nodeUrl, ipfsUrl }: { nodeUrl: string; ipfsUrl: string },
      onConnected?: () => void,
      onDisconnected?: () => void,
      options?: ApiOptions
   ): Promise<AsylumApi> {
      const provider = new WsProvider(nodeUrl)
      onConnected && provider.on('connected', onConnected)
      onDisconnected && provider.on('disconnected', onDisconnected)

      this.provider = provider

      try {
         this.api = await ApiPromise.create({
            provider,
            throwOnConnect: true,
            throwOnUnknown: true,
            types: apiTypes,
            ...options,
         })
         this.ipfsUrl = ipfsUrl
      } catch (e) {
         await provider.disconnect()
         throw e
      }

      return this
   }

   /**
    * Manual disconnect from node initiated by web client.
    */
   async disconnect(): Promise<void> {
      if (this.api) {
         await this.api.disconnect()
      }
      if (this.provider) {
         await this.provider.disconnect()
      }
   }

   /**
    * Getter for polkadot api
    */
   get polkadotApi(): ApiPromise {
      if (!this.api) {
         throw new Error('Api is not loaded')
      }
      return this.api
   }

   /**
    * Sets keyring pair as preferred authorization method during each transaction
    * @param keyringPair {@link KeyringPair} generated from secret
    */
   withKeyringPair(keyringPair: KeyringPair): AsylumApi {
      this.injectedSigner = undefined
      this.keyringPair = keyringPair
      return this
   }

   /**
    * Sets PolkadotJS extension as a preferred authorization method during each transaction
    * @param address
    * @param signer
    */
   withInjectedSigner(address: string, signer: Signer): AsylumApi {
      this.keyringPair = undefined
      this.address = address
      this.injectedSigner = signer
      return this
   }

   /**
    * Uploads metadata object to IPFS in JSON format
    * @param metadata any stringifiable object
    * @return CID string for accessing uploaded object
    * @example
    * ```ts
    * // returns CID string
    * uploadMetadata({name: John Doe, description: average user})
    * ```
    */
   async uploadMetadata(metadata: object): Promise<string> {
      const ipfs = create({
         url: this.ipfsUrl,
      })
      const { cid } = await ipfs.add(JSON.stringify(metadata))

      return cid.toString()
   }

   /**
    * Uploads metadata object to IPFS in JSON format
    * @param metadata
    * @deprecated
    */
   async getMetadataCID(metadata: object): Promise<string> {
      const ipfs = create({
         url: this.ipfsUrl,
      })
      const { cid } = await ipfs.add(JSON.stringify(metadata), { onlyHash: true })

      return cid.toString()
   }

   /**
    * Uploads file to IPFS
    * @param buffer file in binary format
    * @return CID string for accessing uploaded file
    */
   async uploadFile(buffer: ArrayBuffer): Promise<string> {
      const ipfs = create({
         url: this.ipfsUrl,
      })
      const { cid } = await ipfs.add(buffer)

      return cid.toString()
   }

   /**
    * Method for signing transactions with specified authorization method.
    * Keyring or signer should be specified separately before use.
    * @private
    * @param tx Extrinsic created by blockchain plugin that will be passed to node,
    */
   signAndSendWrapped<ApiType extends ApiTypes>(
      tx: SubmittableExtrinsic<ApiType>
   ): Promise<SubmittableResult> {
      return new Promise((resolve, reject) => {
         try {
            if (this.keyringPair) {
               tx.signAndSend(
                  this.keyringPair!,
                  handleTxCallback(resolve, reject, this.api!.registry)
               )
            } else if (this.injectedSigner && this.address) {
               tx.signAndSend(
                  this.address,
                  { signer: this.injectedSigner },
                  handleTxCallback(resolve, reject, this.api!.registry)
               )
            } else {
               reject(new Error('No keyringPair or injectedSigner provided'))
            }
         } catch (e) {
            reject(e)
         }
      })
   }

   /**
    * Creates space on a specified node.
    * @param id id for the space
    * @param admins array of admins for the space
    * @param price price for space passes, that will be minted for accessing the space
    * @see AsylumApi#spacePasses
    */
   async createSpace(id: number, admins: string[], price: number): Promise<SubmittableResult> {
      return this.signAndSendWrapped(this.api!.tx.asylumSpaces.createSpace(id, admins, price))
   }

   /**
    * Connects space to metadata, created separately and adds title and genre.
    * @param id id of the space you want to saturate with the metadata
    * @param cid CID of core data which should be uploaded separately
    * @param title title of the space
    * @param genre genre of the space
    * @see AsylumApi#createSpace
    */
   async setSpaceMetadata(
      id: number,
      cid: string,
      title: string,
      genre: string
   ): Promise<SubmittableResult> {
      return this.signAndSendWrapped(
         this.api!.tx.asylumSpaces.setSpaceMetadata(id, cid, title, genre)
      )
   }

   /**
    * Adds blueprint support for space.
    * @param spaceId id of the space to add blueprint support
    * @param blueprintId id of the blueprint
    */
   async addBlueprintSupport(spaceId: string, blueprintId: number): Promise<SubmittableResult> {
      return this.signAndSendWrapped(
         this.api!.tx.asylumSpaces.addBlueprintSupport(spaceId, blueprintId)
      )
   }

   /**
    * Queries all available spaces from the selected node.
    */
   async spaces(): Promise<Space[]> {
      const entries = await this.api!.query.asylumSpaces.space.entries()
      return mapEntries(entries)
   }

   /**
    * Queries exact space from the selected node.
    * @param id Id of the space to be fetched
    */
   async space(id: number): Promise<Space> {
      const space = await this.api!.query.asylumSpaces.space(id)
      return space.toHuman() as unknown as Space
   }

   /**
    * Queries metadata of the space
    * @param id id of the space
    */
   async spaceMetadataOf(id: number): Promise<SpaceMetadata> {
      const result = await this.api!.query.asylumSpaces.spaceMetadataOf(id)
      return result.toHuman() as unknown as SpaceMetadata
   }

   /**
    * Queries all available space passes on the node. Should be filtered afterwards for specific user if needed.
    */
   async spacePasses(): Promise<any[]> {
      const entries = await this.api!.query.asylumSpaces.spacePass.entries()
      return mapEntries(entries)
   }

   /**
    * Queries all tags on the node. Mostly needed for interpretation manipulations.
    * @see createInterpretationTag
    */
   async tags(): Promise<Tag[]> {
      const entries = await this.api!.query.asylumBlueprints.tags.entries()
      return mapEntries(entries)
   }

   /**
    * Queries all interpretations for the exact blueprint. Blueprint always has last interpretation updates unlike items
    * @param id id of the blueprint
    */
   async blueprintInterpretations(id: string): Promise<Interpretation[]> {
      const result = await this.api!.query.asylumBlueprints.blueprintIntepretations.entries(id)
      return mapEntries(result, (data) => {
         const jsonData = data.toHuman() as any
         jsonData.interpretationInfo = jsonData.interpretation
         delete jsonData.interpretation
         return jsonData
      })
   }

   /**
    * Adds tag to the node,
    * @param tag brief name for the tag, must be unique
    * @param metadata additional metadata for the tag. Usually stringified object.
    */
   async createInterpretationTag(tag: TagName, metadata: string): Promise<SubmittableResult> {
      const tx = this.api!.tx.asylumBlueprints.createInterpretationTag(tag, metadata)
      return this.signAndSendWrapped(tx)
   }

   /**
    * Creates blueprint on the node. Blueprint may be used for minting items after creation.
    * @param blueprintName name of the blueprint, only latin letters
    * @param metadata stringified object with any additional info for the blueprint
    * @param max maximum number of NFTs that can be minted from that blueprint
    * @param interpretations interpretations for usage of minted items in different spaces, extendable
    * @see updateBlueprint
    * @see mintItem
    */
   async createBlueprint(
      blueprintName: string,
      metadata: string,
      max: number | undefined,
      interpretations: InterpretationCreate[]
   ): Promise<SubmittableResult> {
      const tx = this.api!.tx.asylumBlueprints.createBlueprint(
         blueprintName,
         metadata,
         max,
         interpretations
      )
      return this.signAndSendWrapped(tx)
   }

   /**
    * Updates blueprint. Usually adding interpretations.
    * @param blueprintId id of the blueprint to be edited
    * @param changeSet changes for the blueprint
    */
   async updateBlueprint(blueprintId: number, changeSet: ChangeSet): Promise<SubmittableResult> {
      return this.signAndSendWrapped(
         this.api!.tx.asylumBlueprints.updateBlueprint(
            blueprintId,
            changeSet.map((x) => x.convert(this.api!))
         )
      )
   }

   /**
    * Fetches exact blueprint
    * @param id
    */
   async blueprint(id: string): Promise<Blueprint> {
      const blueprint: any = (await this.api!.query.rmrkCore.collections(id)).toHuman()
      return {
         ...omit('symbol', blueprint),
         id,
         name: blueprint.symbol,
      } as Blueprint
   }

   /**
    * Queries all blueprints on connected node.
    */
   async blueprints(): Promise<Blueprint[]> {
      const entries = await this.api!.query.rmrkCore.collections.entries()
      return mapEntries(entries, (data) => {
         const json = data.toHuman() as any
         return {
            ...omit('symbol', json),
            name: json.symbol,
            nftsCount: parseInt(json.nftsCount),
         }
      })
   }

   /**
    * Queries all blueprints created by specified user
    * @param issuer
    */
   async blueprintsByIssuer(issuer: string): Promise<Blueprint[]> {
      const blueprints = await this.blueprints()
      return blueprints.filter((blueprint) => blueprint.issuer === issuer)
   }

   /**
    * Mints item from the blueprint. May be called only by blueprint owner
    * @param owner owner of the item (the one for whom item will be minted for)
    * @param blueprintId id of the blueprint for item minting
    * @param metadata any additional metadata in a string format for the item, differs from the blueprint metadata, may uniquely describe exact item.
    * @see createBlueprint
    * @see items
    */
   async mintItem(
      owner: string,
      blueprintId: string,
      metadata: string
   ): Promise<SubmittableResult> {
      const tx = this.api!.tx.asylumBlueprints.mintItem(owner, blueprintId, metadata)
      return this.signAndSendWrapped(tx)
   }

   /**
    * Accepts updates from blueprint if it was modified by the owner, like added or edited interpretations
    * Two parameters needed as key for item identification
    * @param blueprintId id of the blueprint used for item minting
    * @param itemId id of the item
    */
   async acceptItemUpdate(blueprintId: string, itemId: string): Promise<SubmittableResult> {
      const tx = this.api!.tx.asylumBlueprints.approvePendingChanges(blueprintId, itemId)
      return this.signAndSendWrapped(tx)
   }

   /**
    * Fetches exact item
    * Two parameters needed as key for item identification
    * @param blueprintId id of the blueprint
    * @param id id of the item
    */
   async item(blueprintId: string, id: string): Promise<Item> {
      const item: any = (await this.api!.query.rmrkCore.nfts(blueprintId, id)).toHuman()
      return {
         ...item,
         id,
         blueprintId,
      } as Item
   }

   /**
    * Queries all items minter from the selected blueprint, including those with straggled blueprint interpretations.
    * @param blueprintId id of the blueprint
    */
   async itemsByBlueprint(blueprintId: string): Promise<Item[]> {
      const entries = await this.api!.query.rmrkCore.nfts.entries(blueprintId)
      return mapEntries(entries, itemConverter)
   }

   /**
    * Queries all items existed on node.
    */
   async items(): Promise<Item[]> {
      const entries = await this.api!.query.rmrkCore.nfts.entries()
      return mapEntries(entries, itemConverter)
   }

   /**
    * Queries items of the specific user
    * @param ownerAccountId id of the user
    */
   async itemsByOwner(ownerAccountId: string): Promise<Item[]> {
      const allItems = await this.items()
      return allItems.filter((item) => item.owner.AccountId === ownerAccountId)
   }

   /**
    * Queries item interpretations, including those which was edited by blueprint owner and wasn't accepted
    * Two parameters needed as key for item identification
    * @param blueprintId id of the blueprint
    * @param nftId id of the item
    */
   async itemInterpretations(blueprintId: string, nftId: string): Promise<Interpretation[]> {
      const interpretationTags =
         await this.api!.query.asylumBlueprints.itemInterpretationTags.entries(blueprintId, nftId)
      const interpretations = await this.api!.query.rmrkCore.resources.entries(blueprintId, nftId)
      const mappedInterpretations = mapEntries(interpretations)

      return interpretationTags.map(([key, exposure]) => {
         const interpretation = mappedInterpretations.find(
            (entry) => entry.id.split(':')[2] === (key.toHuman() as any[])[2]
         )
         const {
            id,
            pending,
            resource: {
               Basic: { metadata, src },
            },
         } = interpretation

         return {
            ...(exposure.toHuman() as object),
            id,
            pending,
            interpretationInfo: {
               metadata,
               src,
            },
         } as Interpretation
      })
   }
}

export default new AsylumApi()
