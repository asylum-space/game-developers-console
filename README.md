# Asylum UI

Contains libraries and web apps for interaction with Asylum node.

#### Creator studio

Web app, written in React and designed for space developers to create and manage Asylum entities: create item blueprints, manage interpretations, mint NFTs.

[See details](/packages/creator-studio/README.md)


#### Connection Library 

Connection Library is a typescript library in form of npm package, which ease integration with Asylum pallets providing clear API

[See details](/packages/connection-library/README.md)
